package at.univie.dse.ms4_cli;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import model.BankAccount;
import model.BankOperations;
import model.BankTransaction;
import model.CalculationRequest;
import model.ServiceDescriptor;
import model.ServiceType;

public class CLI {

	private static RestTemplate restTemplate = new RestTemplate();
	private static String serviceController;
	private static final Logger LOG = LoggerFactory.getLogger(CLI.class);

	/**
	 * starts console input
	 * 
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		if (args.length == 0) {
			System.err.println("Missing URL for Service Controller");
			return;
		} else {
			serviceController = args[0];
		}
		System.out.println("Distributed Calculation Cloud" + '\n');
		System.out.println("Hello user, write help to see all possible commands" + '\n');
		startConsoleInput();

	}

	private static void startConsoleInput() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter new command!");
		try {
			String i = sc.nextLine();
			getConsoleCommand(i);

		} catch (NumberFormatException nfe) {
			System.err.println("Invalid Format!");
			startConsoleInput();

		} catch (IOException e) {
			System.err.println("Invalid Input. " + e.getMessage());
		}
	}

	/**
	 * list of all commands that can be executed from the command line
	 * 
	 * @param command
	 * @throws IOException
	 */
	private static void getConsoleCommand(String command) throws IOException {
		switch (command) {
		case "createUser":
			createUserFromConsole();
			break;
		case "createBankAccount":
			createBankAccFromConsole();
			break;
		case "addition":
			additionCalculation();
			break;
		case "multiplication":
			multiplicationCalculation();
			break;
		case "fibonacci":
			fibonacciCalculation();
			break;
		case "prime":
			primeCalculation();
			break;
		case "addMoney":
			addMoneyToAccount();
			break;
		case "withdrawMoney":
			withdrawMoneyFromAccount();
			break;
		case "displayAccounts":
			displayAccounts();
			break;
		case "displayBankAccount":
			displayBankAccount();
			break;
		case "displayUser":
			displayUser();
			break;
		case "displayUsers":
			displayUsers();
			break;
		case "deleteUsers":
			deleteUsers();
			break;
		case "deleteUser":
			deleteUser();
			break;
		case "deleteBankAccountsOfUser":
			deleteAllBankAcc();
			break;
		case "deleteBankAccount":
			deleteBankAccount();
			break;
		case "result":
			getResult();
			break;
		case "help":
			helpCommand();
			break;
		case "exit":
			System.exit(0);
		default:
			System.out.println("Wrong Command! Please try again.");
		}
		startConsoleInput();
	}

	/**
	 * sends info from command line to ServicesController the user writes first name
	 * and second name
	 * 
	 * @throws IOException
	 *             Invalid Input
	 */
	private static void createUserFromConsole() throws IOException {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("DSC: Please enter your First Name: ");
			String firstName = sc.nextLine();
			System.out.println("DSC: Please enter your Second Name: ");
			String secondName = sc.nextLine();

			if (firstName.equals("") && secondName.equals("")) {
				System.out.println("Empty Strings!");
			} else {
				try {
					UriComponentsBuilder builder = UriComponentsBuilder
							.fromUriString(serviceController + "/operationInBank")
							// Add query parameter
							.queryParam("firstName", firstName).queryParam("secondName", secondName);
					ResponseEntity<String> result = restTemplate.exchange(
							serviceController + "/createUser?firstName=" + firstName + "&secondName=" + secondName,
							HttpMethod.POST, null, String.class);
					if (result.getStatusCode().equals(HttpStatus.OK)) {
						System.out.println("New user with id: " + result.getBody() + " was added!");
						startConsoleInput();

					} else {
						System.out.println("User was not created!");
						startConsoleInput();
					}
				} catch (Exception e) {
					LOG.info("User was not created. Invalid arguments.");
					startConsoleInput();
				}
			}

		} catch (NumberFormatException nfe) {
			System.err.println("Names should be String Format!");
		}
	}

	/*
	 * sends info from command line to ServicesController creates new bank account
	 * using the userID
	 * 
	 * @throws IOException
	 */
	private static void createBankAccFromConsole() throws IOException {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("DSC: Please enter your UserID: ");
			String userId = sc.nextLine();
			if (userId.equals("")) {
				System.out.println("Empty UserID!");
			} else {
				try {
					ResponseEntity<String> result = restTemplate.exchange(
							serviceController + "/createAccount/" + userId, HttpMethod.POST, null, String.class);
					if (result.getStatusCode().equals(HttpStatus.OK)) {
						System.out.println("New bankAccount with id: " + result.getBody() + " was added!");
						startConsoleInput();
					} else {
						System.out.println("Account was not created!");
						startConsoleInput();
					}
				} catch (Exception e) {
					LOG.info("There is no such user. You can not add a bank account.");
					startConsoleInput();
				}
			}

		} catch (NumberFormatException nfe) {
			System.err.println("ID should be String Format!");
		}

	}

	/*
	 * sends info from command line to ServicesController executes addition
	 * calculation task
	 * 
	 * @throws IOException
	 */
	private static void additionCalculation() throws IOException {
		System.out.println("Right now " + getAllServices(ServiceType.Addition).length
				+ " calculation services are running. Please choose one to proceed." + '\n');
		showServiceCharges(ServiceType.Addition);
		Scanner sc = new Scanner(System.in);
		try {

			System.out.println("DCS: Write the index of the choosen service" + '\n');
			int index = sc.nextInt();
			System.out.println("DSC: Please enter your UserID: " + '\n');
			String userID = sc.nextLine();
			if (userID.equals("")) {
				System.out.println("Empty UserID!");
				System.out.println("DSC: Please enter your UserID: " + '\n');
				userID = sc.nextLine();
				;
			}
			System.out.println("DSC: Please enter your First Number: " + '\n');
			// String next = sc.nextLine();

			List<BigDecimal> numbers = new ArrayList<>();
			boolean check = true;
			while (check) {
				String next = sc.next();
				if (next.equals("done")) {
					check = false;
					break;
				}
				double firstNumber = Double.parseDouble(next);
				System.out.println("DSC: Please enter your next Number (to end type 'done'): ");
				numbers.add(BigDecimal.valueOf(firstNumber));
			}

			if (numbers.size() == 0) {
				System.out.println("Empty values for numbers!");
			} else if (numbers.size() < 2) {
				System.out.println("At least 2 numbers are needed.");
			} else {
				CalculationRequest request = new CalculationRequest(ServiceType.Addition, userID, numbers);
				UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(serviceController + "/calculations")
						// Add query parameter
						.pathSegment(String.valueOf(index));
				ResponseEntity<String> result = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
						new HttpEntity<>(request), String.class);

				if (result.getStatusCode().equals(HttpStatus.OK)) {
					System.out.println("The ID of the request calculation result is: " + result.getBody());

					// charge the user
					if (getOnGoingServices(index, ServiceType.Addition) > 5) {
						double amount = 20;

						// check if the user has enough money in the bank account
						if (checkBankBalance(amount, userID)) {
							chargeUser(userID, amount);
						} else
							startConsoleInput();
					} else {
						double amount = 10;
						// check if the user has enough money in the bank account
						if (checkBankBalance(amount, userID)) {
							chargeUser(userID, amount);
						} else
							startConsoleInput();
					}
				} else {
					System.out.println("Addition Request was not calculated!");
					startConsoleInput();
				}
			}

		} catch (NumberFormatException nfe) {
			System.err.println("Numbers should be Double Format!");
		}

	}

	/**
	 * sends info from command line to ServicesController executes multiplication
	 * calculation task
	 * 
	 * @throws IOException
	 */
	private static void multiplicationCalculation() throws IOException {
		System.out.println("Right now " + getAllServices(ServiceType.Multiplication).length
				+ " calculation services are running. Please choose one to proceed." + '\n');
		showServiceCharges(ServiceType.Multiplication);
		Scanner sc = new Scanner(System.in);
		try {

			System.out.println("DCS: Write the index of the choosen service");
			int index = sc.nextInt();

			System.out.println("DSC: Please enter your UserID: ");
			String userID = sc.nextLine();
			if (userID.equals("")) {
				System.out.println("Empty UserID!");
				System.out.println("DSC: Please enter your UserID: " + '\n');
				userID = sc.nextLine();
				;
			}
			System.out.println("DSC: Please enter your First Number: ");
			List<BigDecimal> numbers = new ArrayList<>();
			boolean check = true;
			while (check) {
				String next = sc.next();
				if (next.equals("done")) {
					check = false;
					break;
				}
				double firstNumber = Double.parseDouble(next);
				System.out.println("DSC: Please enter your next Number (to end type 'done'): ");
				numbers.add(BigDecimal.valueOf(firstNumber));
			}

			if (numbers.size() == 0) {
				System.out.println("Empty values for numbers!");
			} else if (numbers.size() < 2) {
				System.out.println("At least 2 numbers are needed.");
			} else {
				CalculationRequest request = new CalculationRequest(ServiceType.Multiplication, userID, numbers);
				UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(serviceController + "/calculations")
						// Add query parameter
						.pathSegment(String.valueOf(index));
				ResponseEntity<String> result = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
						new HttpEntity<>(request), String.class);

				if (result.getStatusCode().equals(HttpStatus.OK)) {
					System.out.println("The ID of the request calculation result is: " + result.getBody());
					// charge the user
					if (getOnGoingServices(index, ServiceType.Addition) > 5) {
						double amount = 20;

						// check if the user has enough money in the bank account
						if (checkBankBalance(amount, userID)) {
							chargeUser(userID, amount);
						} else
							startConsoleInput();
					} else {
						double amount = 10;
						// check if the user has enough money in the bank account
						if (checkBankBalance(amount, userID)) {
							chargeUser(userID, amount);
						} else
							startConsoleInput();
					}
				} else {
					System.out.println("Multiplication Request was not calculated!");
					startConsoleInput();
				}
			}
		} catch (NumberFormatException nfe) {
			System.err.println("Numbers should be Double Format!");
		}

	}

	/**
	 * sends info from command line to ServicesController executes fibonacci
	 * calculation task
	 * 
	 * @throws IOException
	 */
	private static void fibonacciCalculation() throws IOException {
		System.out.println("Right now " + getAllServices(ServiceType.Fibonacci).length
				+ " calculation services are running. Please choose one to proceed." + '\n');
		showServiceCharges(ServiceType.Fibonacci);
		Scanner sc = new Scanner(System.in);
		try {

			System.out.println("DCS: Write the index of the choosen service");
			int index = sc.nextInt();

			System.out.println("DSC: Please enter your UserID: ");
			String userID = sc.nextLine();
			if (userID.equals("")) {
				System.out.println("Empty UserID!");
				System.out.println("DSC: Please enter your UserID: " + '\n');
				userID = sc.nextLine();
				;
			}
			System.out.println("DSC: Please enter your Number: ");
			double firstNumber = sc.nextDouble();
			List<BigDecimal> numbers = new ArrayList<>();
			numbers.add(BigDecimal.valueOf(firstNumber));

			if (firstNumber == 0) {
				System.out.println("Empty value for number!");
			} else {
				CalculationRequest request = new CalculationRequest(ServiceType.Fibonacci, userID, numbers);
				UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(serviceController + "/calculations")
						// Add query parameter
						.pathSegment(String.valueOf(index));
				ResponseEntity<String> result = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
						new HttpEntity<>(request), String.class);

				if (result.getStatusCode().equals(HttpStatus.OK)) {
					System.out.println("The ID of the request calculation result is: " + result.getBody());
					// charge the user
					if (getOnGoingServices(index, ServiceType.Addition) > 5) {
						double amount = 20;

						// check if the user has enough money in the bank account
						if (checkBankBalance(amount, userID)) {
							chargeUser(userID, amount);
						} else
							startConsoleInput();
					} else {
						double amount = 10;
						// check if the user has enough money in the bank account
						if (checkBankBalance(amount, userID)) {
							chargeUser(userID, amount);
						} else
							startConsoleInput();
					}
				} else {
					System.out.println("Fibonacci Request was not calculated!");
					startConsoleInput();
				}
			}

		} catch (NumberFormatException nfe) {
			System.err.println("Numbers should be Double Format!");
		}

	}

	/**
	 * sends info from command line to ServicesController checks if the given number
	 * is prime
	 * 
	 * @throws IOException
	 */
	private static void primeCalculation() throws IOException {
		System.out.println("Right now " + getAllServices(ServiceType.Prime).length
				+ " calculation services are running. Please choose one to proceed." + '\n');
		showServiceCharges(ServiceType.Prime);
		Scanner sc = new Scanner(System.in);
		try {

			System.out.println("DCS: Write the index of the choosen service");
			int index = sc.nextInt();

			System.out.println("DSC: Please enter your UserID: ");
			String userID = sc.nextLine();
			if (userID.equals("")) {
				System.out.println("Empty UserID!");
				System.out.println("DSC: Please enter your UserID: " + '\n');
				userID = sc.nextLine();
				;
			}
			System.out.println("DSC: Please enter your Number: ");
			double firstNumber = sc.nextDouble();
			List<BigDecimal> numbers = new ArrayList<>();
			numbers.add(BigDecimal.valueOf(firstNumber));

			if (firstNumber == 0) {
				System.out.println("Empty value for number!");
			} else {
				CalculationRequest request = new CalculationRequest(ServiceType.Prime, userID, numbers);
				UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(serviceController + "/calculations")
						// Add query parameter
						.pathSegment(String.valueOf(index));
				ResponseEntity<String> result = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
						new HttpEntity<>(request), String.class);

				if (result.getStatusCode().equals(HttpStatus.OK)) {
					System.out.println("The ID of the request calculation result is: " + result.getBody());
					// charge the user
					if (getOnGoingServices(index, ServiceType.Addition) > 5) {
						double amount = 20;

						// check if the user has enough money in the bank account
						if (checkBankBalance(amount, userID)) {
							chargeUser(userID, amount);
						} else
							startConsoleInput();
					} else {
						double amount = 10;
						// check if the user has enough money in the bank account
						if (checkBankBalance(amount, userID)) {
							chargeUser(userID, amount);
						} else
							startConsoleInput();
					}
				}
			}

		} catch (NumberFormatException nfe) {
			System.err.println("Numbers should be Double Format!");
		}

	}

	/**
	 * sends info from command line to ServicesController gets the result from ms2
	 * 
	 * @throws IOException
	 */
	private static void getResult() throws IOException {
		Scanner sc = new Scanner(System.in);
		try {

			System.out.println("DSC: Please enter your the ID of the requested calculation: ");
			String taskID = sc.nextLine();
			System.out.println("DSC: Please enter the type of the requested calculation: ");
			String type = sc.nextLine();
			System.out.println("DSC: Please enter the index of the requested service: ");
			int index = sc.nextInt();

			if (taskID.equals("")) {
				System.out.println("Empty value of id!");
			} else {
				UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(serviceController + "/result")
						// Add query parameter
						.pathSegment(taskID).pathSegment(type).pathSegment(String.valueOf(index));
				// TODO check for 202
				System.out.println("The Result of the task is: "
						+ restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, Object.class).getBody());
				startConsoleInput();
			}

		} catch (NumberFormatException nfe) {
			System.err.println("ID should be String Format!");
		}

	}

	/**
	 * sends info from command line to ServicesController adds money to bank account
	 * by given userId, bankAccountId and amount
	 * 
	 * @throws IOException
	 */
	private static void addMoneyToAccount() throws IOException {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("DSC: Please enter UserID: ");
			String userID = sc.nextLine();
			System.out.println("DSC: Please enter your BankAccountID: ");
			String bankAccountID = sc.nextLine();
			System.out.println("DSC: Please enter the amount that you want to add: ");
			double amount = sc.nextDouble();

			if (userID.equals("") && bankAccountID.equals("")) {
				System.out.println("Empty Strings!");
			} else {
				try {
					BankTransaction transaction = new BankTransaction(BankOperations.ADD, amount);
					UriComponentsBuilder builder = UriComponentsBuilder
							.fromUriString(serviceController + "/operationInBank")
							// Add query parameter
							.pathSegment(userID).pathSegment(bankAccountID);
					HttpEntity<BankTransaction> request = new HttpEntity<>(transaction);
					ResponseEntity<String> result = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT,
							request, String.class);
					System.out.println("You have successfully added " + amount + " euros to your bank account");
				} catch (Exception e) {
					LOG.info("No such user ID or bank account ID");
					startConsoleInput();
				}
			}

		} catch (NumberFormatException nfe) {
			System.err.println("ID-s should be String Format!");
		}
	}

	/**
	 * sends info from command line to ServicesController withdraws money from bank
	 * account by given userId, bankAccountId and amount
	 * 
	 * @throws IOException
	 */
	private static void withdrawMoneyFromAccount() throws IOException {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("DSC: Please enter UserID: ");
			String userID = sc.nextLine();
			System.out.println("DSC: Please enter your BankAccountID: ");
			String bankAccountID = sc.nextLine();
			System.out.println("DSC: Please enter the amount that you want to withdraw: ");
			double amount = sc.nextDouble();
			// String amount = sc.nextLine();

			if (userID.equals("") && bankAccountID.equals("")) {
				System.out.println("Empty Strings!");
			} else {
				ResponseEntity<String> result = null;
				try {
					UriComponentsBuilder builder = UriComponentsBuilder
							.fromUriString(serviceController + "/operationInBank")
							// Add query parameter
							.pathSegment(userID).pathSegment(bankAccountID);
					BankTransaction transaction = new BankTransaction(BankOperations.WITHDRAW, amount);
					HttpEntity<BankTransaction> request = new HttpEntity<>(transaction);
					result = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, request, String.class);
					System.out.println("You have successfully withdrawed " + amount + " euros to your bank account");
				} catch (Exception e) {
					LOG.info("No such user ID or bank account ID.. or withdrawing not possible. See MS3 for details.");
					startConsoleInput();
				}

				// if (result.getStatusCode().equals(HttpStatus.OK)) {
				// try {
				// String balance = restTemplate.exchange(serviceController +
				// "/displayAccounts/" + userID, HttpMethod.GET, null, String.class).getBody();
				// System.out.println("Current bank Account listing: " + balance);
				// }
				// catch (Exception e) {
				// LOG.info("Balance should be double");
				// startConsoleInput();
				// }
				// startConsoleInput();
				// }
				// else {
				// System.out.println("The operation was not possible!");
				// startConsoleInput();
				// }

			}
		} catch (NumberFormatException nfe) {
			System.err.println("ID-s should be String Format!");
		}
	}

	/**
	 * sends info from command line to ServicesController displays all user bank
	 * accounts and their balance by given useriD
	 * 
	 * @throws IOException
	 */
	private static void displayAccounts() throws IOException {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("DSC: Please enter your UserID: ");
			String userId = sc.nextLine();
			if (userId.equals("")) {
				System.out.println("Empty UserID!");
			} else {
				try {
					ResponseEntity<Object> result = restTemplate.exchange(
							serviceController + "/displayAccounts/" + userId, HttpMethod.GET, null, Object.class);
					System.out.println("Your current bankAccounts are: " + result.getBody().toString());
				} catch (Exception e) {
					LOG.info("No such user ID");
					startConsoleInput();
				}

			}

		} catch (NumberFormatException nfe) {
			System.err.println("ID should be String Format!");
		}

	}

	/**
	 * sends info from command line to ServiceConroller displays one specific bank
	 * account of the user
	 * 
	 * @throws IOException
	 */
	private static void displayBankAccount() throws IOException {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("DSC: Please enter your UserID");
			String userId = sc.nextLine();
			System.out.println("DSC: Please enter your bankAccountID");
			String bankAccountId = sc.nextLine();
			if (userId.equals("") || bankAccountId.equals("")) {
				System.out.println("Empty Strings!");
				startConsoleInput();
			} else {
				try {
					ResponseEntity<Object> result = restTemplate.exchange(
							serviceController + "/displayAccount/" + userId + "/bankAccount/" + bankAccountId,
							HttpMethod.GET, null, Object.class);
					System.out.println("Your bank account is: " + result.getBody().toString());
				} catch (Exception e) {
					LOG.info("No such user ID or bankAccountID");
					startConsoleInput();
				}

			}

		} catch (NumberFormatException nfe) {
			System.err.println("ID should be String Format!");
		}

	}

	/**
	 * sends info from the command line to ServicesController displays all the
	 * users, which are currently in the program
	 * 
	 * @throws IOException
	 */
	private static void displayUser() throws IOException {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("DSC: Please enter your UserID");
			String userId = sc.nextLine();
			if (userId.equals("")) {
				System.out.println("Empty userID!");
				startConsoleInput();
			} else {
				try {
					ResponseEntity<Object> result = restTemplate.exchange(serviceController + "/displayUser/" + userId,
							HttpMethod.GET, null, Object.class);
					System.out.println("Information about user: " + result.getBody().toString());
				} catch (Exception e) {
					LOG.info("No such user ID");
					startConsoleInput();
				}

			}

		} catch (NumberFormatException nfe) {
			System.err.println("ID should be String Format!");
		}

	}

	/**
	 * sends info from the command line to ServicesController displays info about
	 * specific user
	 * 
	 * @throws IOException
	 */
	private static void displayUsers() throws IOException {
		try {
			ResponseEntity<Object> result = restTemplate.exchange(serviceController + "/displayUsers", HttpMethod.GET,
					null, Object.class);
			System.out.println("Information about all users: " + result.getBody().toString());
		} catch (Exception e) {
			LOG.info("Something went wrong. Please try again!");
			startConsoleInput();
		}
	}

	/**
	 * sends info from the command line to ServicesController deletes all users
	 * 
	 * @throws IOException
	 */
	private static void deleteUsers() throws IOException {
		try {
			restTemplate.exchange(serviceController + "/delete/users", HttpMethod.DELETE, null, Object.class);
			System.out.println("All users have been successfully deleted. ");
		} catch (Exception e) {
			LOG.info("Users could not be deleted because they do not exist.");
			startConsoleInput();
		}
	}

	/**
	 * sends info from the command line to ServicesController deletes one specific
	 * user
	 * 
	 * @throws IOException
	 */
	private static void deleteUser() throws IOException {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("DSC: Please enter your UserID");
			String userId = sc.nextLine();
			if (userId.equals("")) {
				System.out.println("Empty userID!");
				startConsoleInput();
			} else {
				try {
					UriComponentsBuilder builder = UriComponentsBuilder
							.fromUriString(serviceController + "/deleteUser/")
							// Add query parameter
							.pathSegment(userId);
					restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, null, Object.class);
					System.out.println("User has been successfully deleted: ");
				} catch (Exception e) {
					LOG.info("Wrong userID or bankAccountID. Please try again!");
					startConsoleInput();
				}
			}

		} catch (NumberFormatException nfe) {
			System.err.println("ID should be String Format!");
		}
	}

	/**
	 * sends info from the command line to ServicesController deletes all bank
	 * accounts
	 */
	private static void deleteAllBankAcc() {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("DSC: Please enter your UserID");
			String userId = sc.nextLine();
			if (userId.equals("")) {
				System.out.println("Empty userID!");
				startConsoleInput();
			} else {
				try {
					UriComponentsBuilder builder = UriComponentsBuilder
							.fromUriString(serviceController + "/deleteBankAccounts/")
							// Add query parameter
							.pathSegment(userId);
					restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, null, Object.class);
					System.out.println("All bank accounts have been successfully deleted. ");
				} catch (Exception e) {
					LOG.info("Wrong userID or all bank accounts have already been deleted. See MS3 for details.");
					startConsoleInput();
				}
			}

		} catch (NumberFormatException nfe) {
			System.err.println("ID should be String Format!");
		}
	}

	/**
	 * sends info from the command line to ServicesController deletes one specific
	 * bank account
	 */
	private static void deleteBankAccount() {
		Scanner sc = new Scanner(System.in);
		try {
			System.out.println("DSC: Please enter your UserID");
			String userId = sc.nextLine();
			System.out.println("DSC: Please enter your bankAccoutID");
			String bankAccountId = sc.nextLine();
			if (userId.equals("")) {
				System.out.println("Empty userID!");
				startConsoleInput();
			} else {
				try {
					UriComponentsBuilder builder = UriComponentsBuilder
							.fromUriString(serviceController + "/deleteBankAccount/")
							// Add query parameter
							.pathSegment(userId).pathSegment(bankAccountId);
					restTemplate.exchange(builder.toUriString(), HttpMethod.DELETE, null, Object.class);
					System.out.println("Bank account has been successfully deleted. ");
				} catch (Exception e) {
					LOG.info("Wrong userID or bankAccountID. Please try again!");
					startConsoleInput();
				}
			}

		} catch (NumberFormatException nfe) {
			System.err.println("ID should be String Format!");
		}
	}

	/**
	 * sends info from command line to ServicesController withdraws money from the
	 * user's bank account after the specific calculation task has been executed
	 * 
	 * @param userID
	 * @param amount
	 */
	private static void chargeUser(String userID, double amount) {

		String bankAccountID = "";
		// get the id of the first bank account of the user
		try {
			ResponseEntity<BankAccount[]> resultFromDisplay = restTemplate.exchange(
					serviceController + "/displayAccounts/" + userID, HttpMethod.GET, null, BankAccount[].class);
			bankAccountID = resultFromDisplay.getBody()[0].getBankAccountId();
		} catch (Exception e) {
			LOG.info("No such user ID or the user has no current bank acccount");
			startConsoleInput();
		}
		// withdraw
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(serviceController + "/operationInBank")
				// Add query parameter
				.pathSegment(userID).pathSegment(bankAccountID);

		BankTransaction transaction = new BankTransaction(BankOperations.WITHDRAW, amount);
		HttpEntity<BankTransaction> request = new HttpEntity<>(transaction);
		ResponseEntity<String> resultFromWithdraw = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT,
				request, String.class);
		try {
			if (resultFromWithdraw.getStatusCode().equals(HttpStatus.OK)) {
				System.out.println("You have been charged with " + amount);
				startConsoleInput();
			}

			else {
				System.out.println("The operation was not possible!");
				startConsoleInput();
			}
		} catch (Exception e) {
			LOG.info("No such user ID");
			startConsoleInput();
		}

	}

	/**
	 * sends info from command line to ServicesController checks if the user has
	 * enough money in his account
	 * 
	 * @param amount
	 * @param userID
	 * @return
	 */
	private static boolean checkBankBalance(double amount, String userID) {

		double balance = 0;
		// get the id of the first bank account of the user
		try {
			ResponseEntity<BankAccount[]> resultFromDisplay = restTemplate.exchange(
					serviceController + "/displayAccounts/" + userID, HttpMethod.GET, null, BankAccount[].class);
			balance = resultFromDisplay.getBody()[0].getBalance();
		} catch (Exception e) {
			LOG.info("No such user ID or the user has no current bank acccount");
			startConsoleInput();
		}
		if (balance < amount) {

			System.out.println("You dont have enough money in your account. To boost your account write addMoney");
			return false;
		} else
			return true;

	}

	/**
	 * sends info from the command line to ServicesController gets all API-s of the
	 * running services
	 * 
	 * @param type
	 * @return
	 */
	private static ServiceDescriptor[] getAllServices(ServiceType type) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(serviceController + "/getAllAPI")
				// Add query parameter
				.pathSegment(type.toString());
		ResponseEntity services = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null,
				ServiceDescriptor[].class);
		return (ServiceDescriptor[]) services.getBody();
	}

	/**
	 * sends info from the command line to ServicesController string based method,
	 * showing info how many calculation services are running, their ongoing
	 * calculations and the costs of each service
	 * 
	 * @param type
	 */
	private static void showServiceCharges(ServiceType type) {
		ServiceDescriptor[] services = getAllServices(type);
		for (int i = 0; i < services.length; i++) {
			int numberServices = getOnGoingServices(i, type);
			if (numberServices > 5) {
				System.out.println("Service with index " + i + " costs: 20 euros" + '\n');
			} else
				System.out.println("Service with index " + i + " costs: 10 euros" + '\n');

		}
	}

	/**
	 * sends info from the command line to ServicesController gets number of ongoing
	 * services from ms2
	 * 
	 * @param index
	 * @param type
	 * @return
	 */
	private static int getOnGoingServices(int index, ServiceType type) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(serviceController + "/ongoingCalculations")
				// Add query parameter
				.pathSegment(type.toString()).pathSegment(String.valueOf(index));
		ResponseEntity services = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, Integer.class);
		return (int) services.getBody();
	}

	/**
	 * help command, showing all possible commands
	 */
	private static void helpCommand() {
		System.out.println("createUser --------> creates new user" + '\n');
		System.out.println("createBankAccount ------> creates new bank account" + '\n');
		System.out.println("addition -------> calculates new addition operation" + '\n');
		System.out.println("multiplication ------> calculates new multiplication operation" + '\n');
		System.out.println("fibonacci --------> calculates new fibonacci operation" + '\n');
		System.out.println("prime -------> checks if a number is prime" + '\n');
		System.out.println("addMoney ------> adds money to your bank account" + '\n');
		System.out.println("withdrawMoney ------> withdraws money from your bank account" + '\n');
		System.out.println("displayAccounts -----> shows your current bank accounts balance" + '\n');
		System.out.println("displayBankAccount -----> shows a bank account you want to see" + '\n');
		System.out.println("displayUsers -----> shows every user in the bank" + '\n');
		System.out.println("displayUser -----> shows your personal data + bank accounts" + '\n');
		System.out.println("deleteBankAccount -----> deletes a certain bank account" + '\n');
		System.out.println("deleteUser -----> deletes a user from the bank" + '\n');
		System.out.println("deleteBankAccountsOfUser -----> deletes all bank accounts of a certain user" + '\n');
		System.out.println("deleteUsers -----> deletes all users in the bank" + '\n');
		System.out.println(
				"result -----> retrieves the result using calculationID and type (Addition, Prime, Fibonacci, Multiplication)"
						+ '\n');
		System.out.println("exit ------> exit" + '\n');
	}

}
