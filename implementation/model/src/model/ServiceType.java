package model;

/**
 * Enumeration of all possible service types in our system.
 * 
 * @author Martin Hronsky, 01634403
 *
 */
public enum ServiceType {

	Calculation, Addition, Multiplication, Prime, Fibonacci, Billing, SCS, Dashboard

}
