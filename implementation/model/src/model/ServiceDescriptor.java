package model;

/**
 * Class representing a service.
 * 
 * @author Martin Hronsky, 01634403
 *
 */
public class ServiceDescriptor {

	private ServiceType type;
	private String endpoint;

	// For serialization purposes
	public ServiceDescriptor() {
	}

	public ServiceDescriptor(ServiceType type, String endpoint) {
		super();
		this.type = type;
		this.endpoint = endpoint;
	}

	public ServiceType getType() {
		return type;
	}

	public void setType(ServiceType type) {
		this.type = type;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endpoint == null) ? 0 : endpoint.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceDescriptor other = (ServiceDescriptor) obj;
		if (endpoint == null) {
			if (other.endpoint != null)
				return false;
		} else if (!endpoint.equals(other.endpoint))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ServiceDescriptor [type=" + type + ", endpoint=" + endpoint + "]";
	}
}
