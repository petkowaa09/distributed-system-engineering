package model;

import java.util.ArrayList;
import java.util.UUID;


/**
 * Model class for the User 
 * 
 * @author Maria Petrova, 01628066
 *
 */
public class User {
	private String firstName;
	private String secondName;
	private String userId;
	private ArrayList<BankAccount> bankAccounts;
	
	/**
	 * Constructor for creating a user with first and second names
	 * 
	 * @param firstName
	 * @param secondName
	 */
	public User(String firstName, String secondName) {
		this.setFirstName(firstName);
		this.setSecondName(secondName);
		this.userId = UUID.randomUUID().toString();
		this.bankAccounts = new ArrayList<>();
	}

	/**
	 * Retrieving the first name of the user
	 * 
	 * @return String
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Retrieving the second name of the user
	 * 
	 * @return String
	 */
	public String getSecondName() {
		return secondName;
	}

	/**
	 * Retrieving the id of the user
	 * 
	 * @return String
	 */
	public String getUserId() {
		return userId;
	}	
	
	/**
	 * Setting the first name of the user
	 * @param firstName String should not be empty 
	 */
	public void setFirstName(String firstName) {
		if(firstName != null && firstName.isEmpty()) {
			throw new IllegalArgumentException("First name is invalid");
		}
		this.firstName = firstName;
	}

	/**
	 * Setting the second name of the user
	 * @param secondName String should not be empty 
	 */
	public void setSecondName(String secondName) {
		if(secondName != null && secondName.isEmpty()) {
			throw new IllegalArgumentException("Second name is invalid");
		}
		this.secondName = secondName;
	}

	/**
	 * Adding a new bank account to the ArrayList of the User
	 * 
	 * @param bankAcc BankAccount
	 */
	public void addBankAccount(BankAccount bankAcc) {
		this.bankAccounts.add(bankAcc);
	}
	
	/**
	 * Removing a bank account from the ArrayList with the help of its id
	 * 
	 * @param bankAccountId String
	 */
	public void removeBankAccount(String bankAccountId) {
		for(int i = 0; i < this.bankAccounts.size(); i++) {
			if(this.bankAccounts.get(i).getBankAccountId().equals(bankAccountId)) {
				this.bankAccounts.remove(i);
				break;
			}
		}
	}
	
	/**
	 * Retrieving all bank accounts of the user
	 * 
	 * @return ArrayList<BankAccount>
	 */
	public ArrayList<BankAccount> getBankAccounts() {
		return bankAccounts;
	}
	
	/**
	 * 
	 * Retrieving a single bank account of the user by using its id
	 * 
	 * @param bankAccountId String
	 * @return BankAccount
	 */
	public BankAccount getBankAccount(String bankAccountId) {
		BankAccount bankAcc = null;
		for(int i = 0; i < this.bankAccounts.size(); i++) {
			if(this.bankAccounts.get(i).getBankAccountId().equals(bankAccountId)) {
				bankAcc = this.bankAccounts.get(i);
				break;
			}
		}
		return bankAcc;
	}
	
	/**
	 * Deleting all bank accounts of the user
	 */
	public void deleteAllBankAccountsOfUser() {
		this.bankAccounts.clear();
	}
	
	/**
	 * Adding money to a specific bank account
	 * 
	 * @param bankAccId String
	 * @param money double
	 */
	public void addMoneyToBankAccount(String bankAccId, double money) {
		for(int i = 0; i < this.bankAccounts.size(); i++) {
			if(this.bankAccounts.get(i).getBankAccountId().equals(bankAccId)) {
				double currentBalance = this.bankAccounts.get(i).getBalance();
				this.bankAccounts.get(i).setBalance(currentBalance + money);
				break;
			}
		}
	}
	
	/**
	 * Withdrawing money from a specific bank account
	 * 
	 * @param bankAccId String
	 * @param money double
	 */
	public void withdrawMoneyFromBankAccount(String bankAccId, double money){
		for(int i = 0; i < this.bankAccounts.size(); i++) {
			if(this.bankAccounts.get(i).getBankAccountId().equals(bankAccId)) {
				double currentBalance = this.bankAccounts.get(i).getBalance();

				if((currentBalance - money) > 0 ) {
					this.bankAccounts.get(i).setBalance(currentBalance - money);
					break;
				}
				else {
					throw new IllegalArgumentException("Withdrawing is not possible. Account balance sinks under 0.");
				}
			
			}
		}
	}
}
