package model;

/**
 * Enum for the type of bank operations that could be done in MS3
 * 
 * @author Maria Petrova, 01628066
 *
 */

public enum BankOperations {
	ADD, WITHDRAW
}
