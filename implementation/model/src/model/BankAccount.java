package model;

import java.util.UUID;

/**
 * Model class for the Bank Account 
 * 
 * @author Maria Petrova, 01628066
 *
 */
public class BankAccount {
	private double balance;
	private String bankAccountId;
	
	/**
	 * Constructor for creating a empty bank account
	 */
	public BankAccount() {
		this.setBalance(0);
		this.bankAccountId = UUID.randomUUID().toString();
	}

	/**
	 * Retrieving the balance of the bank account
	 * 
	 * @return double
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * Retrieving the id of the bank account
	 * 
	 * @return String
	 */
	public String getBankAccountId() {
		return bankAccountId;
	}

	/**
	 * Setting new balance of the bank account
	 * 
	 * @param balance double
	 */
	public void setBalance(double balance) {
		if(balance < 0) {
			throw new IllegalArgumentException("Balance is not valid");
		}
		this.balance = balance;
	}
}
