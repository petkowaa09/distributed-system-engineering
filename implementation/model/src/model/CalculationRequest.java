package model;

import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.List;

/**
 * Calculation Request Model structure
 *
 * @author Roman Szabo, 01638811
 */

public class CalculationRequest {

	private ServiceType type;
	private String userId;
	private List<BigDecimal> request;

	public CalculationRequest() {}

	public CalculationRequest(ServiceType type, String userId, List<BigDecimal> request) {
		if (type == ServiceType.Billing || type == ServiceType.SCS || type == ServiceType.Dashboard || type == ServiceType.Calculation) {
			throw new InputMismatchException("Invalid Service Type");
		}
		else {
			this.type = type;
			this.userId = userId;
			this.request = request;
		}
	}

	public ServiceType getType() {
		return type;
	}

	public void setType(ServiceType type) {
		this.type = type;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<BigDecimal> getRequest() {
		return request;
	}

	public void setRequest(List<BigDecimal> request) {
		this.request = request;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder("CalculationRequest: [serviceType = " + type + ", userId = " + userId + ", request = {");
		for (BigDecimal bd : request) {
			s.append(bd);
			s.append(", ");
		}
		s.append("}]");
		return s.toString();
	}
}
