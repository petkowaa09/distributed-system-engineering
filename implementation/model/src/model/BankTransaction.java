package model;

/**
 * Class handling the type of transaction done in MS3
 * 
 * @author Maria Petrova, 01628066
 *
 */

public class BankTransaction {
	private BankOperations bankOperation;
	private double amount;
	
	public BankTransaction() {
		
	}
	
	/**
	 * Constructor for creating a bank transaction with parameters
	 * 
	 * @param bankOperation instance of {@link BankOperation}, providing info about what the operation is
	 * @param amount double
	 */
	public BankTransaction(BankOperations bankOperation, double amount) {
		this.bankOperation = bankOperation;
		this.amount = amount;
	}

	/**
	 * Retrieving the bank operation
	 * @return BankOperation
	 */
	public BankOperations getBankOperation() {
		return bankOperation;
	}
	
	/**
	 * Retrieving the amount of money 
	 * 
	 * @return double
	 */
	public double getAmount() {
		return amount;
	}
	
	/**
	 * Setting a bank operation
	 * 
	 * @param bankOperation BankOperation
	 */
	public void setBankOperation(BankOperations bankOperation) {
		this.bankOperation = bankOperation;
	}
	
	/**
	 * Setting amount of money
	 * 
	 * @param amount double
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
}
