package at.univie.dse.ms5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import model.ServiceDescriptor;
import model.ServiceType;

/**
 * Repository for the MS5, storing currently active services and also their
 * history.
 * 
 * @author Martin Hronsky, 01634403
 *
 */
public class LandscapeInfoRepository {

	@Value("${ms1.url}")
	private String serviceRepositoryURL;

	private static final Logger LOG = LoggerFactory.getLogger(LandscapeInfoRepository.class);

	private static final Integer NUM_OF_RECORDS = 13;

	private static final String ALL_SERVICES_URL = "/getAllServices";
	private static final String CALCULATIONS_URL = "/get_ongoing_calculations";

	private Set<ServiceDescriptor> activeServices;
	private Map<ServiceDescriptor, Queue<Integer>> serviceHistory;

	public LandscapeInfoRepository() {
		this.activeServices = Collections.synchronizedSet(new HashSet<ServiceDescriptor>());
		this.serviceHistory = Collections.synchronizedMap(new HashMap<ServiceDescriptor, Queue<Integer>>());
	}

	@Scheduled(fixedDelay = 5000, initialDelay = 5000)
	public void update() {
		RestTemplate rest = new RestTemplate();
		try {
			ResponseEntity<Set<ServiceDescriptor>> response = rest.exchange(serviceRepositoryURL + ALL_SERVICES_URL,
					HttpMethod.GET, null, new ParameterizedTypeReference<Set<ServiceDescriptor>>() {
					});

			Set<ServiceDescriptor> currentlyActiveServices = response.getBody();
			activeServices = filterCalculationServices(currentlyActiveServices);

			// Step 1 : remove all history records for services not contained in this
			// response

			Set<ServiceDescriptor> currentlyStoredHistories = new HashSet<ServiceDescriptor>(serviceHistory.keySet());

			currentlyStoredHistories.removeAll(activeServices);

			for (ServiceDescriptor descriptor : currentlyStoredHistories) {
				serviceHistory.remove(descriptor);
			}

			// Step 2 : create or update records
			for (ServiceDescriptor descriptor : activeServices) {
				// Calculate histories only for supported types
				if (descriptor.getType() == ServiceType.Calculation) {
					if (!serviceHistory.containsKey(descriptor)) {
						Queue<Integer> queue = new CircularFifoQueue<Integer>(NUM_OF_RECORDS);
						for (int i = 0; i < NUM_OF_RECORDS; i++) {
							queue.add(0);
						}
						serviceHistory.put(descriptor, queue);
					}
					try {
						ResponseEntity<Integer> response2 = rest
								.getForEntity(descriptor.getEndpoint() + CALCULATIONS_URL, Integer.class);
						serviceHistory.get(descriptor).add(response2.getBody());
					} catch (ResourceAccessException e) {
						LOG.warn("Calculation service with endpoint {} unreachable, probably died.",
								descriptor.getEndpoint());
					}
				}
			}
		} catch (ResourceAccessException e) {
			LOG.warn("MS1 unreachable...");
		}
	}

	private Set<ServiceDescriptor> filterCalculationServices(Set<ServiceDescriptor> currentlyActiveServices) {
		Set<ServiceDescriptor> setToReturn = new HashSet<ServiceDescriptor>();
		for (ServiceDescriptor descriptor : currentlyActiveServices) {
			ServiceType type = descriptor.getType();
			if (type == ServiceType.Addition || type == ServiceType.Multiplication || type == ServiceType.Fibonacci
					|| type == ServiceType.Prime) {
				setToReturn.add(new ServiceDescriptor(ServiceType.Calculation, descriptor.getEndpoint()));
			} else {
				setToReturn.add(descriptor);
			}
		}
		return setToReturn;
	}

	public Set<ServiceDescriptor> getActiveServices() {
		return activeServices;
	}

	public List<Integer> getServiceHistory(ServiceDescriptor descriptor) {
		if (serviceHistory.containsKey(descriptor)) {
			return new ArrayList<Integer>(serviceHistory.get(descriptor));
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This service became unavailable!");
		}
	}
}
