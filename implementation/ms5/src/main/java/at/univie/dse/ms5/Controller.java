package at.univie.dse.ms5;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import model.ServiceDescriptor;
import model.ServiceType;

/**
 * Controller for the REST API exposed by this service.
 * 
 * @author Martin Hronsky, 01634403
 *
 */
@RestController
public class Controller {

	@Autowired
	LandscapeInfoRepository repository;

	private static final Logger LOG = LoggerFactory.getLogger(Controller.class);

	/**
	 * Used to retrieve all currently active services in the system
	 * 
	 * @return a {@link Set} of {@link ServiceDescriptor} of active services
	 */
	@GetMapping("/getActiveServices")
	public Set<ServiceDescriptor> getActiveServices() {
		Set<ServiceDescriptor> setToReturn = repository.getActiveServices();
		LOG.info("Retrieved {} active service(s).", setToReturn.size());
		return setToReturn;
	}

	/**
	 * Returns a history of a single service identified by its type and endpoint.
	 * History of a service is a list of integers of length 13. Each integer
	 * represents amount of running calculations on that service at specific time.
	 * The step between two measurements is 5 seconds, so that the whole history
	 * shows one minute. The first element in the list is the oldest, the last
	 * element the newest measurement.
	 * 
	 * @param type
	 *            type of the service
	 * @param endpoint
	 *            endpoint of the service
	 * @return a {@link List} of integers
	 * @throws UnsupportedEncodingException
	 */
	@GetMapping("/getServiceHistory/{type}/{endpoint}")
	public List<Integer> getServiceHistory(@PathVariable ServiceType type, @PathVariable String endpoint)
			throws UnsupportedEncodingException {
		String decodedEndpoint = new String(Base64.getDecoder().decode(endpoint));
		List<Integer> listToReturn = repository.getServiceHistory(new ServiceDescriptor(type, decodedEndpoint));
		LOG.info("History of service of type {} with endpoint {} is: {}", type, endpoint, listToReturn);
		return listToReturn;
	}

}
