package at.univie.dse.ms4;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Ms4Application {

	public static void main(String[] args) throws IOException {
		ConfigurableApplicationContext ctx = SpringApplication.run(Ms4Application.class, args);
		ServiceUtils utils = ctx.getBean(ServiceUtils.class);
		utils.registerService(ctx.getEnvironment());
	}
}
