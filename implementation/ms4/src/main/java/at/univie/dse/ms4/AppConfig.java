package at.univie.dse.ms4;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {

	@Bean
	public ServiceUtils serviceUtils() {
		return new ServiceUtils();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
