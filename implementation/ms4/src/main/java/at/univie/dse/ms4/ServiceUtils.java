package at.univie.dse.ms4;

import model.ServiceDescriptor;
import model.ServiceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import java.net.InetAddress;

public class ServiceUtils {

	@Value("${ms1.url}")
	private String serviceRepositoryURL;

	@Autowired
	private RestTemplate restTemplate;

	private static final Logger LOG = LoggerFactory.getLogger(ServiceUtils.class);
	private static final String REGISTER_URL = "/register";

	/**
	 * MS4 registers in Service Repository in MS1
	 * 
	 * @param environment
	 */
	public void registerService(Environment environment) {
		LOG.info("Registering MS4...");
		try {
			String serviceEndpoint = "http://" + InetAddress.getLocalHost().getHostAddress() + ":"
					+ environment.getProperty("local.server.port");
			LOG.info("Service endpoint will be {}", serviceEndpoint);

			ServiceDescriptor serviceDescr = new ServiceDescriptor(ServiceType.SCS, serviceEndpoint);
			restTemplate.postForObject(serviceRepositoryURL + REGISTER_URL, serviceDescr, Boolean.class);
		} catch (Exception e) {
			LOG.error("Failed to register this service, exiting");
			LOG.error(e.getMessage());
			System.exit(45);
		}
	}

}
