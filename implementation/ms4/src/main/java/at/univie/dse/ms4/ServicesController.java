package at.univie.dse.ms4;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import model.BankAccount;
import model.BankTransaction;
import model.CalculationRequest;
import model.ServiceDescriptor;
import model.ServiceType;

@RestController
public class ServicesController {

	@Value("${ms1.url}")
	private String serviceRepositoryUrl;

	private static RestTemplate restTemplate = new RestTemplate();

	private static final Logger LOG = LoggerFactory.getLogger(ServicesController.class);

	// used reference
	// https://stackoverflow.com/questions/38372422/how-to-post-form-data-with-spring-resttemplate?fbclid=IwAR24J7u7yrOYfy-ciKvq6o2kSGrhImQpKqbdkM-8VeMJwkBw2h4ZVtJvAeE
	/**
	 * connects to ms3 endpoint, which creates new user HttpMethod - POST
	 * 
	 * @param firstName
	 * @param secondName
	 * @return ResponseEntity<String> - 200 OK
	 * @throws IOException
	 */
	@PostMapping("/createUser")
	public ResponseEntity<String> createUser(@RequestParam String firstName, @RequestParam String secondName)
			throws IOException {
		String endpoint = getAPI(ServiceType.Billing, 0).toString();
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(endpoint + "/users");
		builder.queryParam("firstName", firstName);
		builder.queryParam("secondName", secondName);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<String> response;
		String results;
		try {
			response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.POST, requestEntity,
					String.class);
			results = response.getBody().toString();
			LOG.debug("The id of the user is: {}", results);
		} catch (Exception e) {
			LOG.info("User was not created. Invalid arguments.");
			LOG.debug("The exception was: ", e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(results, HttpStatus.OK);
	}

	/**
	 * connects to ms3 endpoint, which creates new bank account HttpMethod - POST
	 * 
	 * @param userID
	 * @return ResponseEntity<String> - 200 OK
	 * @throws IOException
	 */
	@PostMapping("/createAccount/{userID}")
	public ResponseEntity<String> createAccount(@PathVariable String userID) throws IOException {
		String endpoint = getAPI(ServiceType.Billing, 0).toString();
		String response;
		try {
			LOG.debug("Endpoint of the service is {}", getAPI(ServiceType.Billing, 0).toString());
			response = restTemplate.postForObject(endpoint + "/users/" + userID + "/bankAccount", userID, String.class);
			LOG.info("The id of the bankAccount is: {}", response);
		} catch (Exception e) {
			LOG.info("There is no such user. You can not add a bank account.");
			LOG.debug("The exception was: ", e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping("/calculations/{index}")
	public ResponseEntity<String> postCalculation(@RequestBody CalculationRequest request, @PathVariable int index)
			throws IOException {
		String endpoint = getAPI(request.getType(), index);
		String calculationId = restTemplate.postForObject(endpoint + "/" + request.getType(), request, String.class);
		return new ResponseEntity<>(calculationId, HttpStatus.OK);
	}

	/**
	 * connects to ms2 endpoint, which get results from the calculation operation
	 * HttpMethod - GET
	 *
	 * @param calculationId
	 *            calculationId
	 * @return ResponseEntity<Object> - 200 OK
	 * @throws IOException
	 * @throws InterruptedException
	 */
	@GetMapping("/result/{calculationId}/{type}/{index}")
	public ResponseEntity<Object> showResults(@PathVariable String calculationId, @PathVariable ServiceType type,
			@PathVariable int index) throws IOException, InterruptedException {
		String endpoint = getAPI(type, index);
		LOG.debug("Endpoint of the service is {}", getAPI(type, 0));
		boolean poll = true;
		Object result = null;
		while (poll) {
			ResponseEntity<Object> response = restTemplate.getForEntity(endpoint + "/get_result/" + calculationId,
					Object.class);
			if (!(response.getStatusCode() == HttpStatus.ACCEPTED)) {
				poll = false;
				result = response.getBody();
			}
			Thread.sleep(500);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	/**
	 * get API from MS1
	 * 
	 * @param type
	 * @return endpoint of the requested service type
	 * @throws IOException
	 */
	@GetMapping("/getAPI/{type}")
	public String getAPI(@PathVariable ServiceType type, int index) throws IOException {
		ServiceDescriptor[] services = restTemplate.getForObject(serviceRepositoryUrl + "/getServices/" + type,
				ServiceDescriptor[].class);
		LOG.debug("Number of Services is: {}", services.length);
		// TODO Billing stuff
		return services[index].getEndpoint();
	}

	@GetMapping("/ongoingCalculations/{type}/{index}")
	public ResponseEntity<Integer> getOngoingCalculations(@PathVariable ServiceType type, @PathVariable int index)
			throws IOException {

		String endpoint = getAPI(type, index);
		int ongoing = restTemplate.getForObject(endpoint + "/get_ongoing_calculations", Integer.class);
		return new ResponseEntity<>(ongoing, HttpStatus.OK);
	}

	@GetMapping("/getAllAPI/{type}")
	public ServiceDescriptor[] getMultipleAPIs(@PathVariable ServiceType type) throws IOException {

		ServiceDescriptor[] services = restTemplate.getForObject(serviceRepositoryUrl + "/getServices/" + type,
				ServiceDescriptor[].class);
		LOG.info("Number of Services is: {}", services.length);
		// TODO Billing stuff
		return services;
	}

	/**
	 * connects to ms3 endpoint, which changes money in bank account HttpMethod -
	 * PUT
	 * 
	 * @param userId
	 * @param bankAccountId
	 * @param transaction
	 * @return ResponseEntity<String> - 200 OK
	 * @throws IOException
	 */
	@PutMapping("/operationInBank/{userId}/{bankAccountId}")
	@ResponseBody
	public ResponseEntity<String> changeMoneyInBank(@PathVariable String userId, @PathVariable String bankAccountId,
			@RequestBody BankTransaction transaction) throws IOException {
		String endpoint = getAPI(ServiceType.Billing, 0).toString();
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<BankTransaction> requestEntity = new HttpEntity<>(transaction, headers);
		ResponseEntity<String> account;
		try {
			account = restTemplate.exchange(endpoint + "/users/" + userId + "/bankAccount/" + bankAccountId,
					HttpMethod.PUT, requestEntity, String.class, transaction);
		} catch (Exception e) {
			LOG.info("Money in bank account could not be changed.. See MS3 messages for more info.");
			LOG.debug("The exception was: ", e);
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(account.getBody(), HttpStatus.OK);
	}

	/**
	 * connects to ms3 endpoint, which shows the balance of the all bank accounts of
	 * the user HttpMethod - GET
	 *
	 * @param userId
	 * @return ResponseEntity<Object> - 200 OK
	 * @throws IOException
	 */
	@GetMapping("/displayAccounts/{userId}")
	public ResponseEntity<BankAccount[]> displayAccountsBalance(@PathVariable String userId) throws IOException {
		String endpoint = getAPI(ServiceType.Billing, 0).toString();
		BankAccount[] account;
		try {
			account = restTemplate.getForObject(endpoint + "/users/" + userId + "/bankAccount", BankAccount[].class);
		} catch (Exception e) {
			LOG.info("This user does not exist.");
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException("This user does not exist", e);
		}
		return new ResponseEntity<>(account, HttpStatus.OK);
	}

	/**
	 * connects to ms3 endpoint, which shows the information about one specific bank
	 * account of the user HttpMethod - GET
	 * 
	 * @param userId
	 * @param bankAccountId
	 * @return Object
	 * @throws IOException
	 */
	@GetMapping("displayAccount/{userId}/bankAccount/{bankAccountId}")
	public ResponseEntity<Object> displayAccountBalance(@PathVariable String userId, @PathVariable String bankAccountId)
			throws IOException {
		String endpoint = getAPI(ServiceType.Billing, 0).toString();
		Object result;
		try {
			result = restTemplate.getForObject(endpoint + "/users/" + userId + "/bankAccount/" + bankAccountId,
					Object.class);
		} catch (Exception e) {
			LOG.info("This user or bank account does not exist.");
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException("This user or bank account does not exist.", e);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	/**
	 * connects to ms3 endpoint, which shows information about all bank accounts of
	 * the user HttpMethod - GET
	 * 
	 * @param userID
	 * @return Object
	 * @throws IOException
	 */
	@GetMapping("/displayUser/{userID}")
	public ResponseEntity<Object> displayUser(@PathVariable String userID) throws IOException {
		String endpoint = getAPI(ServiceType.Billing, 0).toString();
		Object user;
		try {
			user = restTemplate.getForObject(endpoint + "/users/" + userID, Object.class);
		} catch (Exception e) {
			LOG.info("This user does not exist.");
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException("This user does not exist.", e);
		}
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	/**
	 * connects to ms3 endpoint, which shows information about all users and their
	 * bank accounts HttpMethod GET
	 * 
	 * @return Object
	 * @throws IOException
	 */
	@GetMapping("/displayUsers")
	public ResponseEntity<Object> displayAllUser() throws IOException {
		String endpoint = getAPI(ServiceType.Billing, 0).toString();
		Object users = restTemplate.getForObject(endpoint + "/users", Object.class);
		return new ResponseEntity<>(users, HttpStatus.OK);
	}

	/**
	 * connects to ms3 endpoint, which deletes user by his userID HttpMethod DELETE
	 * 
	 * @param userID
	 * @return Boolean
	 * @throws IOException
	 */
	@DeleteMapping("/deleteUser/{userID}")
	public ResponseEntity<Boolean> deleteUser(@PathVariable String userID) throws IOException {
		String endpoint = getAPI(ServiceType.Billing, 0).toString();
		ResponseEntity<Boolean> result;
		try {
			result = restTemplate.exchange(endpoint + "/users/" + userID, HttpMethod.DELETE, null, Boolean.class);
		} catch (Exception e) {
			LOG.info("You cannot delete this user because it does not exist.");
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException("You cannot delete this user because it does not exist.", e);
		}
		return new ResponseEntity<>(result.getBody(), HttpStatus.OK);
	}

	/**
	 * connects to ms3 endpoint, which deletes specific bank account for the
	 * specific user
	 * 
	 * @param userID
	 * @param bankAccountID
	 * @return Boolean
	 * @throws IOException
	 */
	@DeleteMapping("/deleteBankAccount/{userID}/{bankAccountID}")
	public ResponseEntity<Boolean> deteleBankAccount(@PathVariable String userID, @PathVariable String bankAccountID)
			throws IOException {
		String endpoint = getAPI(ServiceType.Billing, 0).toString();
		ResponseEntity<Boolean> result;
		try {
			result = restTemplate.exchange(endpoint + "/users/" + userID + "/bankAccount/" + bankAccountID,
					HttpMethod.DELETE, null, Boolean.class);
		} catch (Exception e) {
			LOG.info("You cannot delete this bank account because it does not exist.");
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException("You cannot delete this bank account because it does not exist.", e);
		}
		return new ResponseEntity<>(result.getBody(), HttpStatus.OK);
	}

	/**
	 * connects to ms3 endpoint, which deletes all bank accounts for the specific
	 * user
	 * 
	 * @param userID
	 * @return Boolean
	 * @throws IOException
	 */
	@DeleteMapping("/deleteBankAccounts/{userID}")
	public ResponseEntity<Boolean> deleteAllBankAcc(@PathVariable String userID) throws IOException {
		String endpoint = getAPI(ServiceType.Billing, 0).toString();
		ResponseEntity<Boolean> result;
		try {
			result = restTemplate.exchange(endpoint + "/users/" + userID + "/bankAccount", HttpMethod.DELETE, null,
					Boolean.class);
		} catch (Exception e) {
			LOG.info("You cannot delete all bank accounts of this user because it does not exist.");
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException(
					"You cannot delete all bank accounts of this user because it does not exist.", e);
		}
		return new ResponseEntity<>(result.getBody(), HttpStatus.OK);
	}

	/**
	 * connects to ms3 endpoint, which deletes all users
	 * 
	 * @return Boolean
	 * @throws IOException
	 */
	@DeleteMapping("/delete/users")
	@ResponseBody
	public ResponseEntity<Boolean> deleteAllUser() throws IOException {
		String endpoint = getAPI(ServiceType.Billing, 0).toString();
		ResponseEntity<Boolean> result;
		try {
			result = restTemplate.exchange(endpoint + "/users", HttpMethod.DELETE, null, Boolean.class);
		} catch (Exception e) {
			LOG.info("There are no users to delete.");
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException("There are no users to delete.", e);
		}
		return new ResponseEntity<>(result.getBody(), HttpStatus.OK);
	}

	/**
	 * API of MS1
	 * 
	 * @return
	 */
	@GetMapping("/heartbeat")
	public ResponseEntity<String> heartbeat() {
		return new ResponseEntity<>("", HttpStatus.OK);
	}

}
