package ms4;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import at.univie.dse.ms4.Ms4Application;
import at.univie.dse.ms4.ServicesController;
import model.ServiceType;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { Ms4Application.class, ServicesController.class })
@WebMvcTest(ServicesController.class)
public class MS4TestSuite {
	@Autowired
	private MockMvc mocker;

	@Test
	public void createUserTest() throws Exception {
		mocker.perform(post("/createUser?firstName=Violeta&secondName=Petkova").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void createBankAccTest() throws Exception {
		MvcResult createUser = mocker.perform(
				post("/createUser?firstName=Violeta&secondName=Petkova").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String userId = createUser.getResponse().getContentAsString();
		mocker.perform(post("/createAccount/{userId}", userId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void calculateAdditionTest() throws Exception {
		String body = "{\n" + "  \"type\" : \"Addition\",\n" + "  \"userId\" : \"Violeta\",\n"
				+ "  \"request\" : [10,10]\n" + "}";
        ServiceType type = ServiceType.Addition;
		mocker.perform(post("/calculations/{type}", type).content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void calculateMultiplicationTest() throws Exception {
		String body = "{\n" + "  \"type\" : \"Multiplication\",\n" + "  \"userId\" : \"Violeta\",\n"
				+ "  \"request\" : [10,10]\n" + "}";

		mocker.perform(post("/calculataions/Multiplication").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void calculateFibonacciTest() throws Exception {
		String body = "{\n" + "  \"type\" : \"Fibonacci\",\n" + "  \"userId\" : \"Violeta\",\n"
				+ "  \"request\" : [10]\n" + "}";

		mocker.perform(post("/calculations/Fibonacci").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void calculatePrimeTest() throws Exception {
		String body = "{\n" + "  \"type\" : \"Prime\",\n" + "  \"userId\" : \"Violeta\",\n" + "  \"request\" : [7]\n"
				+ "}";

		mocker.perform(post("/calculations/Prime").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void showResultsTest() throws Exception {
		String body = "{\n" + "  \"type\" : \"Prime\",\n" + "  \"userId\" : \"Violeta\",\n" + "  \"request\" : [7]\n"
				+ "}";

		MvcResult prime = mocker
				.perform(post("/calculateTask/prime").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String taskId = prime.getResponse().getContentAsString();
		mocker.perform(get("showResults/{taskId}", taskId)).andDo(print()).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("true"));

	}

}
