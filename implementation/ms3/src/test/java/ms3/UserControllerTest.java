package ms3;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import at.univie.dse.ms3.Ms3Application;
import at.univie.dse.ms3.UserController;
import at.univie.dse.ms3.UserRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { Ms3Application.class, UserRepository.class })
@WebMvcTest(UserController.class)
public class UserControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void createUserReturnsStatusOkWithoutErrors() throws Exception {
		mockMvc.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		
		//clear users
		mockMvc.perform(MockMvcRequestBuilders.delete("/users").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void createBankAccountReturnsStatusOkWithoutErrors() throws Exception {
		MvcResult createUser = mockMvc
				.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String userId = createUser.getResponse().getContentAsString();

		mockMvc.perform(post("/users/{userId}/bankAccount", userId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		
		//clear users
		mockMvc.perform(MockMvcRequestBuilders.delete("/users").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void getUserReturnsStatusOkWithoutErrors() throws Exception {
		MvcResult createUser = mockMvc
				.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String userId = createUser.getResponse().getContentAsString();

		JSONObject user = new JSONObject();
		JSONArray bankAccs = new JSONArray();
		user.put("firstName", "Maria");
		user.put("secondName", "K");
		user.put("userId", userId);
		user.put("bankAccounts", bankAccs);

		mockMvc.perform(get("/users/{userId}", userId)).andDo(print()).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().json(user.toString()));
		
		//clear users
		mockMvc.perform(MockMvcRequestBuilders.delete("/users").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void getAllUsersReturnsStatusOkWithoutErrors() throws Exception {
		MvcResult createUser1 = mockMvc
				.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		MvcResult createUser2 = mockMvc
				.perform(post("/users?firstName=M&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String userId1 = createUser1.getResponse().getContentAsString();
		String userId2 = createUser2.getResponse().getContentAsString();

		JSONArray allUsers = new JSONArray();

		JSONObject user1 = new JSONObject();
		JSONArray bankAccs1 = new JSONArray();
		user1.put("firstName", "Maria");
		user1.put("secondName", "K");
		user1.put("userId", userId1);
		user1.put("bankAccounts", bankAccs1);

		JSONObject user2 = new JSONObject();
		JSONArray bankAccs2 = new JSONArray();
		user2.put("firstName", "M");
		user2.put("secondName", "K");
		user2.put("userId", userId2);
		user2.put("bankAccounts", bankAccs2);

		allUsers.put(user1);
		allUsers.put(user2);
		mockMvc.perform(get("/users")).andDo(print()).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().json(allUsers.toString()));
		
		//clear users
		mockMvc.perform(MockMvcRequestBuilders.delete("/users").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void getBankAccountReturnsStatusOkWithoutErrors() throws Exception {
		MvcResult createUser = mockMvc
				.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String userId = createUser.getResponse().getContentAsString();

		MvcResult createBankAccount = mockMvc
				.perform(post("/users/{userId}/bankAccount", userId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String bankAccId = createBankAccount.getResponse().getContentAsString();

		JSONObject bankAcc = new JSONObject();

		bankAcc.put("balance", 0);
		bankAcc.put("bankAccountId", bankAccId);

		mockMvc.perform(get("/users/{userId}/bankAccount/{bankAccId}", userId, bankAccId)).andDo(print())
				.andExpect(status().isOk()).andExpect(MockMvcResultMatchers.content().json(bankAcc.toString()));
		
		//clear users
		mockMvc.perform(MockMvcRequestBuilders.delete("/users").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void getAllBankAccountsOfUserReturnsStatusOkWithoutErrors() throws Exception {
		MvcResult createUser = mockMvc
				.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String userId = createUser.getResponse().getContentAsString();

		MvcResult createBankAccount1 = mockMvc
				.perform(post("/users/{userId}/bankAccount", userId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		MvcResult createBankAccount2 = mockMvc
				.perform(post("/users/{userId}/bankAccount", userId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String bankAccId1 = createBankAccount1.getResponse().getContentAsString();
		String bankAccId2 = createBankAccount2.getResponse().getContentAsString();

		JSONArray allBankAcc = new JSONArray();

		JSONObject bankAcc1 = new JSONObject();
		bankAcc1.put("balance", 0);
		bankAcc1.put("bankAccountId", bankAccId1);

		JSONObject bankAcc2 = new JSONObject();
		bankAcc2.put("balance", 0);
		bankAcc2.put("bankAccountId", bankAccId2);

		allBankAcc.put(bankAcc1);
		allBankAcc.put(bankAcc2);

		mockMvc.perform(get("/users/{userId}/bankAccount", userId)).andDo(print()).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().json(allBankAcc.toString()));
		
		//clear users
		mockMvc.perform(MockMvcRequestBuilders.delete("/users").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void addMoneyInBankAccount() throws Exception {
		MvcResult createUser = mockMvc
				.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String userId = createUser.getResponse().getContentAsString();

		MvcResult createBankAccount = mockMvc
				.perform(post("/users/{userId}/bankAccount", userId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String bankAccId = createBankAccount.getResponse().getContentAsString();

		JSONObject bankTransaction = new JSONObject();
		bankTransaction.put("bankOperation", "ADD");
		bankTransaction.put("amount", 20);

		mockMvc.perform(MockMvcRequestBuilders.put("/users/{userId}/bankAccount/{bankAccId}", userId, bankAccId)
				.content(bankTransaction.toString()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andReturn();
		
		//clear users
		mockMvc.perform(MockMvcRequestBuilders.delete("/users").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void withdrawMoneyInBankAccount() throws Exception {
		MvcResult createUser = mockMvc
				.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String userId = createUser.getResponse().getContentAsString();

		MvcResult createBankAccount = mockMvc
				.perform(post("/users/{userId}/bankAccount", userId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String bankAccId = createBankAccount.getResponse().getContentAsString();

		JSONObject addBankTransaction = new JSONObject();
		addBankTransaction.put("bankOperation", "ADD");
		addBankTransaction.put("amount", 20);

		mockMvc.perform(MockMvcRequestBuilders.put("/users/{userId}/bankAccount/{bankAccId}", userId, bankAccId)
				.content(addBankTransaction.toString()).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		JSONObject withDrawBankTransaction = new JSONObject();
		withDrawBankTransaction.put("bankOperation", "WITHDRAW");
		withDrawBankTransaction.put("amount", 10);

		mockMvc.perform(MockMvcRequestBuilders.put("/users/{userId}/bankAccount/{bankAccId}", userId, bankAccId)
				.content(withDrawBankTransaction.toString()).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		
		//clear users
		mockMvc.perform(MockMvcRequestBuilders.delete("/users").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void deleteUserReturnStatusOkWithoutErrors() throws Exception {

		MvcResult createUser = mockMvc
				.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String userId = createUser.getResponse().getContentAsString();

		MvcResult deleteUser = mockMvc.perform(
				MockMvcRequestBuilders.delete("/users/{userId}", userId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void deleteAllUsersReturnsStatusOkWithoutErrors() throws Exception {
		MvcResult createUser1 = mockMvc.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		MvcResult createUser2 = mockMvc.perform(post("/users?firstName=Hi&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		MvcResult deleteUsers = mockMvc.perform(MockMvcRequestBuilders.delete("/users").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void deleteBankAccountReturnsStatusOkWithoutErrors() throws Exception {
		MvcResult createUser = mockMvc
				.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String userId = createUser.getResponse().getContentAsString();

		MvcResult createBankAccount = mockMvc
				.perform(post("/users/{userId}/bankAccount", userId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String bankAccountId = createBankAccount.getResponse().getContentAsString();
		mockMvc.perform(
				MockMvcRequestBuilders.delete("/users/{userId}/bankAccount/{bankAccountId}", userId, bankAccountId)
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
	}

	@Test
	public void deleteAllBankAccountsOfUserReturnsStatusOkWithoutErrors() throws Exception {
		MvcResult createUser = mockMvc
				.perform(post("/users?firstName=Maria&secondName=K").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		String userId = createUser.getResponse().getContentAsString();

		mockMvc.perform(post("/users/{userId}/bankAccount", userId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		mockMvc.perform(post("/users/{userId}/bankAccount", userId).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		mockMvc.perform(MockMvcRequestBuilders.delete("/users/{userId}/bankAccount", userId)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
	}

}