package ms3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import at.univie.dse.ms3.UserRepository;
import model.BankAccount;
import model.User;

public class UserRepositoryTest {

	private UserRepository userRepo = null;

	@BeforeEach
	public void setUp() {
		userRepo = new UserRepository();
	}

	@Test
	public void createUserReturnsId() {
		String userId = userRepo.createUser("Maria", "K");
		assertNotNull(userId);
	}

	@Test
	public void createBankAccountReturnsId() {
		String userId = userRepo.createUser("Maria", "K");
		String bankAccId = userRepo.createBankAccount(userId);
		assertNotNull(bankAccId);
	}

	@Test
	public void getUserReturnsUser() {
		String userId = userRepo.createUser("Maria", "K");
		User user = userRepo.getUser(userId);
		assertNotNull(user);
		assertEquals(userId, user.getUserId());
	}

	@Test
	public void getBankAccountReturnsBankAcc() {
		String userId = userRepo.createUser("Maria", "K");
		String bankAccountId = userRepo.createBankAccount(userId);
		BankAccount bankAcc = userRepo.getBankAccount(userId, bankAccountId);
		assertNotNull(bankAcc);
		assertEquals(bankAccountId, bankAcc.getBankAccountId());
	}

	@Test
	public void getAllUsersReturnsAllUsers() {
		userRepo.createUser("Maria", "K");
		userRepo.createUser("M", "P");
		ArrayList<User> allUsers = userRepo.getAllUsers();

		assertNotNull(allUsers);
		assertEquals(allUsers.size(), 2);
	}

	@Test
	public void getAllBankAccountsOfUserReturnsAllBankAcc() {
		String userId = userRepo.createUser("Maria", "K");
		userRepo.createBankAccount(userId);
		userRepo.createBankAccount(userId);

		ArrayList<BankAccount> allBankAccOfUser = userRepo.getAllBankAccountsOfUser(userId);
		assertNotNull(allBankAccOfUser);

		assertEquals(allBankAccOfUser.size(), 2);
	}

	@Test
	public void addMoneyToBankAccount() {
		String userId = userRepo.createUser("Maria", "K");
		String bankAccountId = userRepo.createBankAccount(userId);

		userRepo.addMoneyToBankAccount(userId, bankAccountId, 20.9);

		BankAccount bankAcc = userRepo.getBankAccount(userId, bankAccountId);
		assertNotNull(bankAcc);
		assertEquals(bankAcc.getBankAccountId(), bankAccountId);
		assertEquals(bankAcc.getBalance(), 20.9);
	}

	@Test
	public void withdrawMoneyFromBankAccountShouldPerformWithdrawing() {
		String userId = userRepo.createUser("Maria", "K");
		String bankAccountId = userRepo.createBankAccount(userId);

		userRepo.addMoneyToBankAccount(userId, bankAccountId, 20);
		userRepo.withdrawMoneyFromBankAccount(userId, bankAccountId, 10);
		BankAccount bankAcc = userRepo.getBankAccount(userId, bankAccountId);
		assertNotNull(bankAcc);
		assertEquals(bankAcc.getBankAccountId(), bankAccountId);
		assertEquals(bankAcc.getBalance(), 10);
	}

	@Test
	public void deleteUserDeletesUser() {
		String userId = userRepo.createUser("Maria", "K");
		boolean checkDelete = userRepo.deleteUser(userId);
		assertTrue(checkDelete);

		ArrayList<User> allUsers = userRepo.getAllUsers();

		assertNotNull(allUsers);
		assertEquals(allUsers.size(), 0);
	}

	@Test
	public void deleteBankAccountDeletesBankAcc() {
		String userId = userRepo.createUser("Maria", "K");
		String bankAccountId = userRepo.createBankAccount(userId);
		boolean checkDelete = userRepo.deleteBankAccount(userId, bankAccountId);
		assertTrue(checkDelete);

		ArrayList<BankAccount> allBankAccOfUser = userRepo.getAllBankAccountsOfUser(userId);
		assertNotNull(allBankAccOfUser);

		assertEquals(allBankAccOfUser.size(), 0);
	}

	@Test
	public void deleteAllBankAccountsOfUserDeletesAllBankAcc() {
		String userId = userRepo.createUser("Maria", "K");
		userRepo.createBankAccount(userId);
		userRepo.createBankAccount(userId);
		boolean checkDelete = userRepo.deleteAllBankAccountsOfUser(userId);
		assertTrue(checkDelete);

		ArrayList<BankAccount> allBankAccOfUser = userRepo.getAllBankAccountsOfUser(userId);
		assertNotNull(allBankAccOfUser);

		assertEquals(allBankAccOfUser.size(), 0);
	}

	@Test
	public void deleteAllUsersDeletesAllUsers() {
		userRepo.createUser("Maria", "K");
		userRepo.createUser("Maria", "p");
		boolean checkDelete = userRepo.deleteAllUsers();
		assertTrue(checkDelete);

		ArrayList<User> allUsers = userRepo.getAllUsers();

		assertNotNull(allUsers);
		assertEquals(allUsers.size(), 0);
	}
}
