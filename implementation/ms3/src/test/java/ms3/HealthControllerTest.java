package ms3;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import at.univie.dse.ms3.HealthController;

public class HealthControllerTest {

	@Test
	public void heartbeatReturnsHttpStatus200() {
		HealthController hc = new HealthController();
		ResponseEntity<?> heartbeat = hc.heartbeat();
		HttpStatus response = heartbeat.getStatusCode();

		assertEquals(response.value(), 200);
	}
}
