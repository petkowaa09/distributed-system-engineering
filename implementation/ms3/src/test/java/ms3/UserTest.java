package ms3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.BankAccount;
import model.User;

public class UserTest {
	private User user = null;
			
	@BeforeEach
	public void setUser() {
		user = new User("M", "P");
	}
	
	@Test
	public void firstNameIsCorrect() {
		assertEquals("M", user.getFirstName());
	}
	
	@Test
	public void secondNameIsCorrect() {
		assertEquals("P", user.getSecondName());
	}
	
	@Test
	public void firstNameShouldThrowIllegalArgumentException() {
		IllegalArgumentException exc = Assertions.assertThrows(IllegalArgumentException.class, () ->{ 
			 user = new User("", "P");
		});
		assertEquals("First name is invalid", exc.getMessage());
	}
	
	@Test
	public void secondNameShouldThrowIllegalArgumentException() {
		IllegalArgumentException exc = Assertions.assertThrows(IllegalArgumentException.class, () ->{ 
			 user = new User("r", "");
		});
		assertEquals("Second name is invalid", exc.getMessage());
	}
	
	@Test
	public void addBankAccountAddsNewBankAcc() {
		BankAccount bankAcc = new BankAccount();
		user.addBankAccount(bankAcc);
		assertNotNull(user.getBankAccount(bankAcc.getBankAccountId()));
		assertEquals(user.getBankAccounts().size(), 1);
	}
	
	@Test
	public void removeBankAccountDeletesBankAcc() {
		BankAccount bankAcc = new BankAccount();
		user.addBankAccount(bankAcc);
		user.removeBankAccount(bankAcc.getBankAccountId());
		assertEquals(user.getBankAccounts().size(), 0);
	}
	
	@Test
	public void getBankAccountsReturnsAllBankAccs() {
		BankAccount bankAcc1 = new BankAccount();
		BankAccount bankAcc2 = new BankAccount();
		user.addBankAccount(bankAcc1);
		user.addBankAccount(bankAcc2);
		ArrayList<BankAccount> allBankAcc = user.getBankAccounts();
		int indexBank1 = allBankAcc.indexOf(bankAcc1);
		int indexBank2 = allBankAcc.indexOf(bankAcc2);
		assertNotNull(allBankAcc);
		assertEquals(allBankAcc.size(), 2);
		assertEquals(bankAcc1, allBankAcc.get(indexBank1));
		assertEquals(bankAcc2, allBankAcc.get(indexBank2));
	}
	
	@Test
	public void getBankAccountReturnsBankAcc() {
		BankAccount bankAcc = new BankAccount();
		user.addBankAccount(bankAcc);
		BankAccount bankAccUser = user.getBankAccount(bankAcc.getBankAccountId());
		assertNotNull(bankAccUser);
		assertEquals(bankAccUser, bankAcc);
	}
	
	@Test
	public void deleteAllBankAccountsOfUserDeletesAllBankAccs() {
		BankAccount bankAcc1 = new BankAccount();
		BankAccount bankAcc2 = new BankAccount();
		user.addBankAccount(bankAcc1);
		user.addBankAccount(bankAcc2);
		user.deleteAllBankAccountsOfUser();
		ArrayList<BankAccount> allBankAcc = user.getBankAccounts();
		assertEquals(allBankAcc.size(), 0);
	}
	
	@Test
	public void addMoneyToBankAccountAddsMoney() {
		BankAccount bankAcc = new BankAccount();
		user.addBankAccount(bankAcc);
		user.addMoneyToBankAccount(bankAcc.getBankAccountId(), 50);
		BankAccount bankAccUser = user.getBankAccount(bankAcc.getBankAccountId());
		assertEquals(bankAccUser.getBalance(), 50);
	}
	
	@Test
	public void withdrawMoneyFromBankAccountShouldThrowIllegalArgumentException(){
		BankAccount bankAcc = new BankAccount();
		user.addBankAccount(bankAcc);
			
		IllegalArgumentException exc = Assertions.assertThrows(IllegalArgumentException.class, () ->{ 
			user.withdrawMoneyFromBankAccount(bankAcc.getBankAccountId(), 10);
		});
		assertEquals("Withdrawing is not possible. Account balance sinks under 0.", exc.getMessage());		
	}
	
	@Test
	public void withdrawMoneyFromBankAccountWithdrawsMoney(){
		BankAccount bankAcc = new BankAccount();
		user.addBankAccount(bankAcc);
		user.addMoneyToBankAccount(bankAcc.getBankAccountId(), 50);	
		user.withdrawMoneyFromBankAccount(bankAcc.getBankAccountId(), 10);
		BankAccount bankAccUser = user.getBankAccount(bankAcc.getBankAccountId());
		assertEquals(bankAccUser.getBalance(), 40);	
	}
	
}
	