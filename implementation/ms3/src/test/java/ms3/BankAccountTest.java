package ms3;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.BankAccount;

public class BankAccountTest {
	private BankAccount bankAccount = null;

	@BeforeEach
	public void setBankAcc() {
		bankAccount = new BankAccount();
	}

	@Test
	public void bankAccBalanceIsCorrect() {
		assertEquals(bankAccount.getBalance(), 0);
	}

	@Test
	public void setBalanceShouldThrowIllegalArgumentException() {
		IllegalArgumentException exc = Assertions.assertThrows(IllegalArgumentException.class, () -> {
			bankAccount.setBalance(-1);
		});
		assertEquals("Balance is not valid", exc.getMessage());
	}

}
