package at.univie.dse.ms3;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller returning the heartbeat of the microService
 * 
 * @author Maria Petrova, 01628066
 *
 */
@RestController
public class HealthController {
	
	/*
	 * Heartbeat method to check if the service is running. For MS1.
	 * 
	 * @return ResponseEntity - 200 OK, Empty Body
	 */
	@GetMapping("/heartbeat")
	public ResponseEntity heartbeat() {
		return new ResponseEntity(HttpStatus.OK);
	}
}
