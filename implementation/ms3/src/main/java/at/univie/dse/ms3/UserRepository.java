package at.univie.dse.ms3;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ResponseStatus;

import at.univie.dse.ms3.exceptions.BadArgumentException;
import model.BankAccount;
import model.User;

/**
 * Repository that stores all the users 
 * 
 * @author Maria Petrova, 01628066
 *
 */

public class UserRepository {
	private HashMap<String, User> allUsers = new HashMap<String, User>();
	
	private static final Logger LOG = LoggerFactory.getLogger(UserRepository.class);

	public UserRepository() {}
		
	/**
	 * Method used for the creation of user
	 * 
	 * @param firstName String
	 * @param secondName String
	 * @return the id of the user
	 */
	public String createUser(String firstName, String secondName) {
		User newUser = null;
		try {
			newUser = new User(firstName, secondName);
		} catch (Exception e) {
			LOG.info(e.getMessage());
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException(e.getMessage(), e);
		}	
		this.allUsers.put(newUser.getUserId(), newUser);
		LOG.info("User created! ID: " + newUser.getUserId());
		return newUser.getUserId();
	}
	
	/**
	 * Method used for the creation of a bank account
	 * 
	 * @param userId, indicating for which user a bank account will be created
	 * @return bank account ID
	 */
	
	public String createBankAccount(String userId) {
		BankAccount newBankAcc = new BankAccount();
		User user = null;
		try {
			user = this.allUsers.get(userId);
			if(user == null) {				
				throw new NullPointerException("There is no such user. You can not add a bank account.");
			}
			user.addBankAccount(newBankAcc);
			LOG.info("Bank account created! ID: " + newBankAcc.getBankAccountId());
			return newBankAcc.getBankAccountId();
			
		} catch (NullPointerException e) {
			LOG.info(e.getMessage());
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException(e.getMessage(), e);
		}	
	}
	
	/**
	 * Retrieving a specific user
	 * 
	 * @param userId String
	 * @return User
	 */
	public User getUser(String userId) {
		try {
			if(this.allUsers.size() == 0) {
				throw new NullPointerException("No users present.");
			}
			if(this.allUsers.get(userId) == null) {
				throw new NullPointerException("This user does not exist!");
			}
		} catch(NullPointerException e) {
			LOG.info(e.getMessage());
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException(e.getMessage(), e);
		}
		return this.allUsers.get(userId);
	}
	
	/**
	 * Method for retrieving a specific bank account
	 * 
	 * @param userId String
	 * @param bankAccountId String
	 * @return BankAccount
	 */
	public BankAccount getBankAccount(String userId, String bankAccountId) {
		BankAccount bankAcc = null;
		try {
			if(this.allUsers.size() == 0) {
				throw new NullPointerException("No users present.");
			}
			if(this.allUsers.get(userId) == null) {
				throw new NullPointerException("This user does not exist! => Bank account also!");
			}
			bankAcc = this.allUsers.get(userId).getBankAccount(bankAccountId);
			if(bankAcc == null) {
				throw new NullPointerException("This bank account does not exist.");
			}
			
		} catch(NullPointerException e) {
			LOG.info(e.getMessage());
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException(e.getMessage(), e);
		}
		return bankAcc;
	}
	
	/**
	 * Retrieving all users
	 * 
	 * @return ArrayList<User>
	 */
	public ArrayList<User> getAllUsers() {
		ArrayList<User> users = new ArrayList<>();
		for (User user : allUsers.values()) {
			users.add(user);
		}
		LOG.info("Found {} user(s)!", users.size());
		return users;
	}
	
	/**
	 * Method for retrieving all bank accounts of a specific user
	 * 
	 * @param userId String
	 * @return ArrayList<BankAccount>
	 */
	public ArrayList<BankAccount> getAllBankAccountsOfUser(String userId) {
		ArrayList<BankAccount> bankAccounts = this.getUser(userId).getBankAccounts();		
		LOG.info("Found {} bank account(s)!", bankAccounts.size());
		return bankAccounts;
	}
	
	/**
	 * Method for adding money in the bank account
	 * 
	 * @param userId String
	 * @param bankAccId String
	 * @param money double
	 */
	public void addMoneyToBankAccount(String userId, String bankAccId, double money) {
		LOG.info("Adding money to bank account... Old balance: " + this.allUsers.get(userId).getBankAccount(bankAccId).getBalance());
		
		this.allUsers.get(userId).addMoneyToBankAccount(bankAccId, money);
		LOG.info("Adding money to bank account completed! New balance: " + this.allUsers.get(userId).getBankAccount(bankAccId).getBalance());
		
	}
	
	/**
	 * Method for withdrawing money from a bank account
	 * @param userId String
	 * @param bankAccId String
	 * @param money double
	 */
	public void withdrawMoneyFromBankAccount(String userId, String bankAccId, double money){
		LOG.info("Withdrawing money from bank account... Old balance: " + this.allUsers.get(userId).getBankAccount(bankAccId).getBalance());
		try {
			this.allUsers.get(userId).withdrawMoneyFromBankAccount(bankAccId, money);
			LOG.info("Withdrawing money from bank account... New balance: " + this.allUsers.get(userId).getBankAccount(bankAccId).getBalance());
		} catch(IllegalArgumentException e) {
			LOG.info(e.getMessage());
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException(e.getMessage(), e);
		}
	}
	
	/**
	 * Method for deleting a single user
	 * 
	 * @param userId String
	 * @return boolean which indicates whether the user was deleted or not
	 */
	public boolean deleteUser(String userId) {

		if(!this.allUsers.containsKey(userId)) {
			return false;
		}
		this.allUsers.remove(userId);	
		return true;
	}

	/**
	 * Method for deleting a single bank account
	 * 
	 * @param userId String
	 * @param bankAccountId String
	 * @return boolean which indicates whether the user was deleted or not
	 */
	public boolean deleteBankAccount(String userId, String bankAccId) {

		if(this.allUsers.get(userId).getBankAccount(bankAccId) == null) {
			return false;
		}
		this.allUsers.get(userId).removeBankAccount(bankAccId);
	
		return true;
	}	
	
	/**
	 * Method for deleting all bank accounts of a single user
	 * 
	 * @param userId String
	 * @return boolean which indicates whether the user was deleted or not
	 */
	public boolean deleteAllBankAccountsOfUser(String userId) {

		if(this.allUsers.get(userId).getBankAccounts().isEmpty()) {
			return false;
		}
		this.allUsers.get(userId).deleteAllBankAccountsOfUser();
	
		return true;
	}	

	/**
	 * Method for deleting all users
	 * 
	 * @return boolean which indicates whether the user was deleted or not
	 */
	public boolean deleteAllUsers() {		
		if(this.allUsers.isEmpty()) {
			return false;
		}
		
		this.allUsers.clear();
		return true;
	}
}
