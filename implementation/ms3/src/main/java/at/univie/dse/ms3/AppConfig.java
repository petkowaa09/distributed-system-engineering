package at.univie.dse.ms3;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Java-based configuration for this service.
 * 
 * @author Maria Petrova, 01628066
 *
 */
@Configuration
public class AppConfig {

	@Bean
	public UserRepository userRepository() {
		return new UserRepository();
	}

	@Bean
	public ServiceUtils serviceUtils() {
		return new ServiceUtils();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
