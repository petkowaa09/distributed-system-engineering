package at.univie.dse.ms3;

import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import model.ServiceDescriptor;
import model.ServiceType;

/**
 * Class used for the registration of the service
 * 
 * @author Maria Petrova, 01628066
 *
 */
public class ServiceUtils {

	@Value("${ms1.url}")
	private String serviceRepositoryURL;

	@Autowired
	private RestTemplate restTemplate;

	private static final Logger LOG = LoggerFactory.getLogger(ServiceUtils.class);
	private static final String REGISTER_URL = "/register";

	/**
	 * Method used for the registration of MS3 in MS1
	 * 
	 * @param environment
	 *            Environment , used to get the port of the current service, in
	 *            order to register it in MS1
	 */
	public void registerService(Environment environment) {
		LOG.info("Registering MS3...");
		try {
			String serviceEndpoint = "http://" + InetAddress.getLocalHost().getHostAddress() + ":"
					+ environment.getProperty("local.server.port");
			LOG.info("Service endpoint will be {}", serviceEndpoint);

			ServiceDescriptor serviceDescr = new ServiceDescriptor(ServiceType.Billing, serviceEndpoint);
			restTemplate.postForObject(serviceRepositoryURL + REGISTER_URL, serviceDescr, Boolean.class);
		} catch (Exception e) {
			LOG.error("Failed to register this service, exiting");
			LOG.error(e.getMessage());
			System.exit(45);
		}
	}

}
