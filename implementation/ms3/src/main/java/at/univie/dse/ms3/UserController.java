package at.univie.dse.ms3;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import at.univie.dse.ms3.exceptions.BadArgumentException;
import model.BankAccount;
import model.BankTransaction;
import model.User;

/**
 * Controller for the REST API exposed by this microservice
 * 
 * @author Maria Petrova, 01628066
 *
 */

@RestController
@RequestMapping(value = "/users")
public class UserController {

	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserRepository userRepository;

	/**
	 * Method used for the creation of user
	 * 
	 * @param firstName String, indicating the first name of the user
	 * @param secondName String, indicating the second name of the user
	 * @return user ID
	 */
	@PostMapping(consumes = "application/json", produces = "application/json")
	@ResponseBody
	public String createUser(@RequestParam String firstName, @RequestParam String secondName) {
		String userId = userRepository.createUser(firstName, secondName);
		return userId;
	}

	/**
	 * Method used for the creation of a bank account
	 * 
	 * @param userId, indicating for which user a bank account will be created
	 * @return bank account ID
	 */
	@PostMapping(value = "/{userId}/bankAccount")
	@ResponseBody
	public String createBankAccount(@PathVariable String userId) {
		String bankAccId = userRepository.createBankAccount(userId);
		return bankAccId;
	}

	/**
	 * Retrieving a specific user
	 * 
	 * @param userId String
	 * @return User
	 */
	@GetMapping("/{userId}")
	@ResponseBody
	public User getUser(@PathVariable String userId) {
		LOG.info("Retrieving a user...");

		return this.userRepository.getUser(userId);
	}

	/**
	 * Retrieving all users
	 * 
	 * @return ArrayList<User>
	 */
	@GetMapping()
	@ResponseBody
	public ArrayList<User> getAllUsers() {
		LOG.info("Retrieving all users...");
		return this.userRepository.getAllUsers();
	}

	/**
	 * Method for retrieving a specific bank account
	 * 
	 * @param userId String
	 * @param bankAccountId String
	 * @return BankAccount
	 */
	@GetMapping("/{userId}/bankAccount/{bankAccountId}")
	@ResponseBody
	public BankAccount getBankAccount(@PathVariable String userId, @PathVariable String bankAccountId) {
		LOG.info("Retrieving a bank account...");

		return this.userRepository.getBankAccount(userId, bankAccountId);
	}
	
	/**
	 * Method for retrieving all bank accounts of a specific user
	 * 
	 * @param userId String
	 * @return ArrayList<BankAccount>
	 */

	@GetMapping("/{userId}/bankAccount")
	@ResponseBody
	public ArrayList<BankAccount> getAllBankAccountsOfUser(@PathVariable String userId) {
		LOG.info("Retrieving all bank accounts of a user...");
		ArrayList<BankAccount> bankAcc = new ArrayList<>();
		try {
			bankAcc = this.userRepository.getAllBankAccountsOfUser(userId);
		} catch (NullPointerException e) {
			LOG.info("There are no Bank Accounts for this user");
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException(e.getMessage(), e);
		}

		return bankAcc;
	}
	
	/**
	 * Method for adding or withdrawing money to/from a specific bank account of a specific user
	 * 
	 * @param userId String
	 * @param bankAccountId String
	 * @param bt instance of {@link BankTransaction}, providing needed
	 *            information about the transaction, whether it is adding or withdrawing and the amount of money
	 */

	@PutMapping("/{userId}/bankAccount/{bankAccountId}")
	@ResponseBody
	public void changeMoneyInBankAccount(@PathVariable String userId, @PathVariable String bankAccountId,
			@RequestBody BankTransaction bt) {
		try {
			switch (bt.getBankOperation()) {
			case ADD:
				this.userRepository.addMoneyToBankAccount(userId, bankAccountId, bt.getAmount());
				break;
			case WITHDRAW:
				this.userRepository.withdrawMoneyFromBankAccount(userId, bankAccountId, bt.getAmount());
				break;
			}
		} catch (NullPointerException e) {
			LOG.info("There is no Bank Account");
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException("There is no Bank Account", e);
		}
	}
	
	/**
	 * Method for deleting a single user
	 * 
	 * @param userId String
	 * @return boolean which indicates whether the user was deleted or not
	 */

	@DeleteMapping("/{userId}")
	@ResponseBody
	public boolean deleteUser(@PathVariable String userId) {
		boolean deleted = false;
		try {
			deleted = this.userRepository.deleteUser(userId);
			if (deleted) {
				LOG.info("User was successfully deleted!");
			} else {
				throw new NullPointerException("User could not be deleted because it does not exist!");
			} 
		} catch (NullPointerException e) {
			LOG.info(e.getMessage());
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException(e.getMessage(), e);
		}
		return deleted;
	}

	/**
	 * Method for deleting all users
	 * 
	 * @return boolean which indicates whether the user was deleted or not
	 */
	@DeleteMapping()
	@ResponseBody
	public boolean deleteAllUsers() {
		boolean deleted = false;
		try {
			deleted = this.userRepository.deleteAllUsers();
			if (deleted) {
				LOG.info("All users were successfully deleted!");
			} else {
				throw new NullPointerException("Users could not be deleted because they do not exist!");
			}
		} catch (NullPointerException e) {
			LOG.info(e.getMessage());
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException(e.getMessage(), e);
		}
		return deleted;
	}

	/**
	 * Method for deleting a single bank account
	 * 
	 * @param userId String
	 * @param bankAccountId String
	 * @return boolean which indicates whether the user was deleted or not
	 */
	@DeleteMapping("/{userId}/bankAccount/{bankAccountId}")
	@ResponseBody
	public boolean deleteBankAccount(@PathVariable String userId, @PathVariable String bankAccountId) {
		boolean deleted = false;
		try {
			deleted = this.userRepository.deleteBankAccount(userId, bankAccountId);		
			if (deleted) {
				LOG.info("Bank Account was successfully deleted!");
			} else {
				throw new NullPointerException("Bank Account could not be deleted because it does not exist!");
			}
		} catch (NullPointerException e) {
			LOG.info(e.getMessage());
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException(e.getMessage(), e);
		}
		return deleted;
	}

	/**
	 * Method for deleting all bank accounts of a single user
	 * 
	 * @param userId String
	 * @return boolean which indicates whether the user was deleted or not
	 */
	@DeleteMapping("/{userId}/bankAccount")
	@ResponseBody
	public boolean deleteAllBankAccountsOfUser(@PathVariable String userId) {
		boolean deleted = false;
		try {
			deleted = this.userRepository.deleteAllBankAccountsOfUser(userId);	
			if (deleted) {
				LOG.info("All Bank Accounts were successfully deleted!");
			} else {
				throw new NullPointerException("All Bank Accounts could not be deleted because they do not exist!");
			}
		} catch (NullPointerException e) {
			LOG.info(e.getMessage());
			LOG.debug("The exception was: ", e);
			throw new BadArgumentException(e.getMessage(), e);
		}
		return deleted;
	}
}
