package at.univie.dse.ms3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Main of MS3
 * 
 * @author Maria Petrova, 01628066
 *
 */

@SpringBootApplication
public class Ms3Application {

	/**
	 * @param args
	 *            String[] used to get the URL of MS1
	 */
	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(Ms3Application.class, args);
		ServiceUtils utils = ctx.getBean(ServiceUtils.class);
		utils.registerService(ctx.getEnvironment());
	}

}
