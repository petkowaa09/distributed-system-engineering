package at.univie.dse.ms3.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception class, used to change the HTTP Status when an exception occurs
 * 
 * @author Maria Petrova, 01628066
 *
 */

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadArgumentException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public BadArgumentException(String errorMessage, Throwable err) {
		super(errorMessage, err);
	}
}

