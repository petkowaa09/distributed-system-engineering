package at.univie.dse.ms1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Java-based configuration for this service.
 * 
 * @author Martin Hronsky, 01634403
 *
 */
@Configuration
@EnableScheduling
public class AppConfig {

	@Bean
	public ServiceRepository serviceRepository() {
		return new ServiceRepository();
	}

}
