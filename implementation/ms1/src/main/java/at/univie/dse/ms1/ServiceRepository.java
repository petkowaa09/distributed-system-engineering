package at.univie.dse.ms1;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import model.ServiceDescriptor;
import model.ServiceType;

/**
 * Service repository used for storing and querying services.
 * 
 * @author Martin Hronsky, 01634403
 *
 */
public class ServiceRepository {

	// Delay for the cleaning sweep of inactive services, in ms
	private static final int INACTIVE_SWEEP_DELAY = 30000;

	private static final String HEARTBEAT_URL = "/heartbeat";

	private Map<ServiceType, Set<ServiceDescriptor>> services;

	private static final Logger LOG = LoggerFactory.getLogger(ServiceRepository.class);

	public ServiceRepository() {
		this.services = Collections.synchronizedMap(new HashMap<ServiceType, Set<ServiceDescriptor>>());
		// Initialize the storage
		for (ServiceType type : ServiceType.values()) {
			services.put(type, Collections.synchronizedSet(new HashSet<ServiceDescriptor>()));
		}
	}

	public Set<ServiceDescriptor> getServices(ServiceType type) {
		Set<ServiceDescriptor> foundServices = services.get(type);
		return foundServices;
	}

	public boolean addService(ServiceDescriptor service) {
		return services.get(service.getType()).add(service);
	}

	/**
	 * Scheduled method that removes all inactive services from the repository.
	 * 
	 */
	@Scheduled(fixedDelay = INACTIVE_SWEEP_DELAY, initialDelay = INACTIVE_SWEEP_DELAY)
	private void removeInactiveServices() throws Exception {
		LOG.info("Removing inactive services...");
		int counter = 0;
		// Create rest client, set timeouts to 500ms for faster sweeps
		RestTemplate rest = new RestTemplateBuilder().setConnectTimeout(Duration.ofMillis(500))
				.setReadTimeout(Duration.ofMillis(500)).build();

		for (ServiceType type : ServiceType.values()) {
			Iterator<ServiceDescriptor> currentServicesItereator = services.get(type).iterator();
			while (currentServicesItereator.hasNext()) {
				ServiceDescriptor service = currentServicesItereator.next();
				try {
					ResponseEntity<String> response = rest.getForEntity(service.getEndpoint() + HEARTBEAT_URL,
							String.class);
					// Sanity check, other statuses should not be possible
					if (!HttpStatus.OK.equals(response.getStatusCode())) {
						throw new ResourceAccessException("HttpStatus was not OK.");
					}
				} catch (ResourceAccessException e) {
					LOG.info("Service running udner {} was deemed inactive, removing...", service.getEndpoint());
					LOG.debug("The exception was: ", e);
					currentServicesItereator.remove();
					counter++;
				}
			}
		}
		LOG.info("Removed {} service(s)!", counter);
	}

	public Set<ServiceDescriptor> getAllServices() {
		Set<ServiceDescriptor> setToReturn = new HashSet<ServiceDescriptor>();
		for (ServiceType type : services.keySet()) {
			setToReturn.addAll(services.get(type));
		}
		return setToReturn;
	}
}
