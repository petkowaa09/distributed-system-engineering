package at.univie.dse.ms1;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import model.ServiceDescriptor;
import model.ServiceType;

/**
 * Controller for the REST API exposed by this service.
 * 
 * @author Martin Hronsky, 01634403
 *
 */
@RestController
public class Controller {

	@Autowired
	private ServiceRepository repository;

	private static final Logger LOG = LoggerFactory.getLogger(Controller.class);

	/**
	 * Service end-point responsible for service registration.
	 * 
	 * @param service
	 *            instance of {@link ServiceDescriptor}, providing needed
	 *            information about the service
	 * @return boolean indicating whether the service was registered or not
	 */
	@PostMapping("/register")
	public boolean registerService(@RequestBody ServiceDescriptor service) {
		boolean wasRegistered = repository.addService(service);
		if (wasRegistered) {
			LOG.info("Service of type {} with endpoint: {} was successfully registered.", service.getType(),
					service.getEndpoint());
		} else {
			LOG.warn("Service with endpoint: {} was not registered, most likely a duplicate.", service.getEndpoint());
		}
		return wasRegistered;
	}

	/**
	 * Service end-point responsible for querying all services.
	 * 
	 * @return a {@link Set} of {@link ServiceDescriptor} for each registered
	 *         service
	 */
	@GetMapping("/getAllServices")
	public Set<ServiceDescriptor> getAllServices() {
		Set<ServiceDescriptor> setToReturn = repository.getAllServices();
		LOG.info("Retrieved {} services.", setToReturn.size());
		return setToReturn;
	}

	/**
	 * Service end-point responsible for querying specific type of services.
	 * 
	 * @param type
	 *            instance of {@link ServiceType}, indicating what type of service
	 *            is queried
	 * @return a {@link Set} of {@link ServiceDescriptor} for each registered
	 *         service of specified type
	 */
	@GetMapping("/getServices/{type}")
	public Set<ServiceDescriptor> getService(@PathVariable ServiceType type) {
		Set<ServiceDescriptor> setToReturn = repository.getServices(type);
		LOG.info("Retrieved {} service(s) of type {}.", setToReturn.size(), type);
		return setToReturn;
	}

}
