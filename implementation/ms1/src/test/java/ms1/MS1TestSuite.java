package ms1;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import at.univie.dse.ms1.Controller;
import at.univie.dse.ms1.Ms1Application;
import at.univie.dse.ms1.ServiceRepository;
import model.ServiceDescriptor;
import model.ServiceType;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { Ms1Application.class, ServiceRepository.class })
@WebMvcTest(Controller.class)
public class MS1TestSuite {

	@Autowired
	private MockMvc mocker;

	private ObjectMapper mapper = new ObjectMapper();

	@Test
	public void ms1Test() throws Exception {

		ServiceDescriptor testDescriptor = new ServiceDescriptor(ServiceType.Billing, "http://localhost:8080");

		String json = mapper.writeValueAsString(testDescriptor);

		mocker.perform(
				post("/register").content(json).characterEncoding("UTF-8").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().string("true"));

		mocker.perform(get("/getAllServices")).andExpect(status().isOk()).andExpect(content().string("[" + json + "]"));

		mocker.perform(get("/getServices/" + testDescriptor.getType())).andExpect(status().isOk())
				.andExpect(content().string("[" + json + "]"));

		mocker.perform(get("/getServices/" + ServiceType.Addition)).andExpect(status().isOk())
				.andExpect(content().string("[]"));

	}

}
