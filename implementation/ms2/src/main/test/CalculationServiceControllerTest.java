import at.univie.dse.ms2.CalculationServiceController;
import at.univie.dse.ms2.Ms2Application;
import at.univie.dse.ms2.ThreadHandler.ThreadHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test class for REST Controller, with error handling Test and Calculation output results.
 *
 * @author Roman Szabo, 01638811
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Ms2Application.class, ThreadHandler.class})
@WebMvcTest(CalculationServiceController.class)
public class CalculationServiceControllerTest {

	@Autowired
	private MockMvc mockMvc;

	/**
	 * Test if service is running.
	 *
	 * @throws Exception from MockMvc
	 */
	@Test
	public void heartBeatTest() throws Exception {

		mockMvc.perform(get("/heartbeat"))
				.andDo(print())
				.andExpect(status().isOk());

	}

	/**
	 * Test call for ongoing calculations
	 * @throws Exception from MockMvc
	 */
	@Test
	public void getOngoingCalculationsTest() throws Exception {

		mockMvc.perform(get("/get_ongoing_calculations"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("0"));
	}

	/**
	 * Test addition call and if result is correct. Calls both /addition and /get_result methods. There is 5 sec. pause before calling for result.
	 * @throws Exception from MockMvc
	 */
	@Test
	public void additionTest() throws Exception {

		String body = "{\n" +
				"  \"type\" : \"Addition\",\n" +
				"  \"userId\" : \"Roman\",\n" +
				"  \"request\" : [2,3]\n" +
				"}";

		MvcResult add = mockMvc.perform(post("/addition").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();

		String calculationId = add.getResponse().getContentAsString();

		Thread.sleep(5000);

		mockMvc.perform(get("/get_result/{calculationId}", calculationId))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("5"));

	}

	/**
	 * Test multiplication call and if result is correct. Calls both /addition and /get_result methods. There is 5 sec. pause before calling for result.
	 * @throws Exception from MockMvc
	 */
	@Test
	public void multiplicationTest() throws Exception {

		String body = "{\n" +
				"  \"type\" : \"Multiplication\",\n" +
				"  \"userId\" : \"Roman\",\n" +
				"  \"request\" : [2,3]\n" +
				"}";

		MvcResult add = mockMvc.perform(post("/multiplication").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();

		String calculationId = add.getResponse().getContentAsString();

		Thread.sleep(5000);

		mockMvc.perform(get("/get_result/{calculationId}", calculationId))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("6"));

	}

	/**
	 * Test prime call and if result is correct. Calls both /addition and /get_result methods. There is 5 sec. pause before calling for result.
	 * @throws Exception from MockMvc
	 */
	@Test
	public void primeTest() throws Exception {

		String body = "{\n" +
				"  \"type\" : \"Prime\",\n" +
				"  \"userId\" : \"Roman\",\n" +
				"  \"request\" : [10]\n" +
				"}";

		MvcResult add = mockMvc.perform(post("/prime").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();

		String calculationId = add.getResponse().getContentAsString();

		Thread.sleep(5000);

		mockMvc.perform(get("/get_result/{calculationId}", calculationId))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("false"));

	}

	/**
	 * Test fibonacci call and if result is correct. Calls both /addition and /get_result methods. There is 5 sec. pause before calling for result.
	 * @throws Exception from MockMvc
	 */
	@Test
	public void fibonacciTest() throws Exception {

		String body = "{\n" +
				"  \"type\" : \"Fibonacci\",\n" +
				"  \"userId\" : \"Roman\",\n" +
				"  \"request\" : [10]\n" +
				"}";

		MvcResult add = mockMvc.perform(post("/fibonacci").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andReturn();

		String calculationId = add.getResponse().getContentAsString();

		Thread.sleep(5000);

		mockMvc.perform(get("/get_result/{calculationId}", calculationId))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().string("[0,1,1,2,3,5,8,13,21,34]"));

	}

	/**
	 * Test Error handling from Controller.
	 * Calls each service once, each call tests another error type.
	 * @throws Exception from MockMvc
	 */
	@Test
	public void BadRequestsTest() throws Exception {

		String body = "{\n" +
				"  \"type\" : \"Fibonacci\",\n" +
				"  \"userId\" : \"Roman\",\n" +
				"  \"request\" : [10, 10]\n" +
				"}";

		mockMvc.perform(post("/addition").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(content().string("Invalid Service Type detected. This is Addition Endpoint"));

		body = "{\n" +
				"  \"type\" : \"Addition\",\n" +
				"  \"userId\" : \"Roman\",\n" +
				"  \"request\" : [10]\n" +
				"}";

		mockMvc.perform(post("/addition").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(content().string("Missing input array of at least 2 numbers."));

		body = "{\n" +
				"  \"type\" : \"Prime\",\n" +
				"  \"userId\" : \"Roman\",\n" +
				"  \"request\" : []\n" +
				"}";

		mockMvc.perform(post("/prime").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(content().string("Missing input array or it is empty."));

		body = "{\n" +
				"  \"type\" : \"Fibonacci\",\n" +
				"  \"userId\" : \"Roman\",\n" +
				"  \"request\" : [10,10]\n" +
				"}";

		mockMvc.perform(post("/fibonacci").content(body).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest())
				.andExpect(content().string("Missing input array of exactly 1 number."));


	}


}
