package at.univie.dse.ms2.Calculation;

import java.math.BigDecimal;
import java.util.List;

/**
 * Class for multiplication calculation
 *
 * @author Roman Szabo, 01638811
 */
class Multiplication {

	/**
	 * Calculate multiplication
	 *
	 * @param request List<BigDecimal>
	 * @return BigDecimal result
	 */
	BigDecimal calculate(List<BigDecimal> request) {
		BigDecimal result = new BigDecimal(1);
		for (BigDecimal d : request) {
			result = result.multiply(d);
		}
		return result;
	}

}
