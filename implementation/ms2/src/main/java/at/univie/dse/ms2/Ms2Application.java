package at.univie.dse.ms2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Ms2Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(Ms2Application.class, args);
		ServiceUtils utils = ctx.getBean(ServiceUtils.class);
		utils.registerService(ctx.getEnvironment());
	}
}
