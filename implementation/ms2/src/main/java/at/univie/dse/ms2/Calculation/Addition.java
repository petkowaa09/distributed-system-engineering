package at.univie.dse.ms2.Calculation;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * Class for addition calculation
 *
 * @author Roman Szabo
 */
class Addition {

	/**
	 * Calculate Addition
	 *
	 * @param request List<BigDecimal>
	 * @return BigDecimal result
	 * @throws IOException exception
	 */
	BigDecimal calculate(List<BigDecimal> request) throws IOException {
		BigDecimal result = new BigDecimal(0);
		for (BigDecimal d : request) {
			result = result.add(d);
		}
		return result;
	}

}
