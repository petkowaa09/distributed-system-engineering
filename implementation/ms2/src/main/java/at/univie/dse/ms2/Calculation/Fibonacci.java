package at.univie.dse.ms2.Calculation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for fibonacci calculation
 *
 * @author Roman Szabo, 01638811
 */
class Fibonacci {

	/**
	 * Calculate fibonacci list up to given length.
	 *
	 * @param request List<BigDecimal>
	 * @return List<BigDecimal> fibonacci list
	 */
	List<BigDecimal> calculate(List<BigDecimal> request) {
		List<BigDecimal> result = new ArrayList<>();
		int fibSequenceLength = request.get(0).intValue();
		BigDecimal prev = new BigDecimal(0);
		BigDecimal next = new BigDecimal(1);

		for (int i = 0; i < fibSequenceLength; i++) {
			result.add(prev);
			BigDecimal sum = prev.add(next);
			prev = next;
			next = sum;
		}
		return result;

	}

}
