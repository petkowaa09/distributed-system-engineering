package at.univie.dse.ms2;

import at.univie.dse.ms2.ThreadHandler.ThreadHandler;
import model.CalculationRequest;
import model.ServiceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Rest Controller for MS2
 *
 * @author Roman Szabo, 01638811
 */
@RestController
public class CalculationServiceController {

	private final ThreadHandler threadHandler;

	private static final Logger LOG = LoggerFactory.getLogger(CalculationServiceController.class);

	public CalculationServiceController(ThreadHandler threadHandler) {
		this.threadHandler = threadHandler;
	}

	/**
	 * Addition service call. Fire & Forget call, which triggers thread controller.
	 *
	 * @param request
	 *            CalculationRequest
	 * @return ResponseEntity<String> - 200 OK, Calculation ID * - 400 BAD REQUEST,
	 *         Error Message
	 */
	@PostMapping("/Addition")
	public ResponseEntity<String> addition(@RequestBody CalculationRequest request) {
		try {
			checkRequest(request, ServiceType.Addition);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

		String id = threadHandler.processRequest(request.getType(), request.getRequest());
		LOG.info("The requested calculation id is: {}", id);
		return new ResponseEntity<>(id, HttpStatus.OK);
	}

	/**
	 * Multiplication service call. Fire & Forget call, which triggers thread
	 * controller.
	 * 
	 * @param request
	 *            CalculationRequest
	 * @return ResponseEntity<String> - 200 OK, Calculation ID * - 400 BAD REQUEST,
	 *         Error Message
	 */
	@PostMapping("/Multiplication")
	public ResponseEntity<String> multiplication(@RequestBody CalculationRequest request) {
		try {
			checkRequest(request, ServiceType.Multiplication);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

		String id = threadHandler.processRequest(request.getType(), request.getRequest());
		LOG.info("The requested calculation id is: {}", id);
		return new ResponseEntity<>(id, HttpStatus.OK);
	}

	/**
	 * Fibonacci service call. Fire & Forget call, which triggers thread controller.
	 * 
	 * @param request
	 *            CalculationRequest
	 * @return ResponseEntity<String> - 200 OK, Calculation ID * - 400 BAD REQUEST,
	 *         Error Message
	 */
	@PostMapping("/Fibonacci")
	public ResponseEntity<String> fibonacci(@RequestBody CalculationRequest request) {
		try {
			checkRequest(request, ServiceType.Fibonacci);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

		String id = threadHandler.processRequest(request.getType(), request.getRequest());
		LOG.info("The requested calculation id is: {}", id);
		return new ResponseEntity<>(id, HttpStatus.OK);
	}

	/**
	 * Prime service call. Fire & Forget call, which triggers thread controller.
	 * 
	 * @param request
	 *            CalculationRequest
	 * @return ResponseEntity<String> - 200 OK, Calculation ID - 400 BAD REQUEST,
	 *         Error Message
	 */
	@PostMapping("/Prime")
	public ResponseEntity<String> prime(@RequestBody CalculationRequest request) {
		try {
			checkRequest(request, ServiceType.Prime);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

		String id = threadHandler.processRequest(request.getType(), request.getRequest());
		LOG.info("The requested calculation id is: {}", id);
		return new ResponseEntity<>(id, HttpStatus.OK);
	}

	/**
	 * Get Result call. Returns Result for given calculation ID obtained by firing
	 * calculation.
	 *
	 * @param calculationId
	 *            unique Calculation ID
	 * @return ResponseEntity - 200 OK, Calculation ID - 202 ACCEPTED, Check Later
	 *         Message - 500 INTERNAL SERVER ERROR, Error Message
	 */
	@GetMapping("/get_result/{calculationId}")
	public ResponseEntity<?> getResult(@PathVariable String calculationId) {
		LOG.info("Getting result for calculationId: {}", calculationId);
		Object result = threadHandler.getResult(calculationId);
		if (result instanceof Exception) {
			return new ResponseEntity<>(((Exception) result).getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (result == null) {
			return new ResponseEntity<>("Given calculation is ongoing. Please check later.", HttpStatus.ACCEPTED);
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}

	/**
	 * Get ongoing calculations for MS4
	 * 
	 * @return ResponseEntity<Integer> - 200 OK, number of calculations
	 */
	@GetMapping("/get_ongoing_calculations")
	public ResponseEntity<Integer> getOngoingCalculations() {
		int ongoing = threadHandler.getRunningThreads();
		return new ResponseEntity<>(ongoing, HttpStatus.OK);
	}

	/**
	 * Heartbeat method to check if service is running. For MS1.
	 * 
	 * @return ResponseEntity - 200 OK, Empty Body
	 */
	@GetMapping("/heartbeat")
	public ResponseEntity<?> heartbeat() {
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Private method for request checking
	 * 
	 * @param request
	 *            CalculationRequest
	 * @param type
	 *            ServiceType triggered
	 * @throws Exception
	 *             Error message in case of validation error.
	 */
	private void checkRequest(CalculationRequest request, ServiceType type) throws Exception {
		if (request == null) {
			throw new Exception("Empty Request Received");
		} else if (request.getType() == null || request.getType() != type) {
			throw new Exception("Invalid Service Type detected. This is " + type + " Endpoint");
		} else if (request.getRequest() == null || request.getRequest().size() == 0) {
			throw new Exception("Missing input array or it is empty.");
		} else if (type == ServiceType.Fibonacci || type == ServiceType.Prime) {
			if (request.getRequest().size() != 1) {
				throw new Exception("Missing input array of exactly 1 number.");
			}
		} else if (type == ServiceType.Addition || type == ServiceType.Multiplication) {
			if (request.getRequest().size() <= 1) {
				throw new Exception("Missing input array of at least 2 numbers.");
			}
		}

	}

}
