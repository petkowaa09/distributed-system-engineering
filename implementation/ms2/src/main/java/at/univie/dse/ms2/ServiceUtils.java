package at.univie.dse.ms2;

import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.client.RestTemplate;

import model.ServiceDescriptor;
import model.ServiceType;

public class ServiceUtils {

	@Value("${ms1.url}")
	private String serviceRepositoryURL;

	@Autowired
	private RestTemplate restTemplate;

	private static final Logger LOG = LoggerFactory.getLogger(ServiceUtils.class);
	private static final String REGISTER_URL = "/register";

	public void registerService(Environment environment) {
		LOG.info("Registering MS2...");
		try {
			String serviceEndpoint = "http://" + InetAddress.getLocalHost().getHostAddress() + ":"
					+ environment.getProperty("local.server.port");
			LOG.info("Service endpoint will be {}", serviceEndpoint);

			ServiceDescriptor serviceDescr = new ServiceDescriptor(ServiceType.Addition, serviceEndpoint);
			restTemplate.postForObject(serviceRepositoryURL + REGISTER_URL, serviceDescr, Boolean.class);
			serviceDescr = new ServiceDescriptor(ServiceType.Multiplication, serviceEndpoint);
			restTemplate.postForObject(serviceRepositoryURL + REGISTER_URL, serviceDescr, Boolean.class);
			serviceDescr = new ServiceDescriptor(ServiceType.Prime, serviceEndpoint);
			restTemplate.postForObject(serviceRepositoryURL + REGISTER_URL, serviceDescr, Boolean.class);
			serviceDescr = new ServiceDescriptor(ServiceType.Fibonacci, serviceEndpoint);
			restTemplate.postForObject(serviceRepositoryURL + REGISTER_URL, serviceDescr, Boolean.class);
		} catch (Exception e) {
			LOG.error("Failed to register this service, exiting");
			LOG.error(e.getMessage());
			System.exit(45);
		}
	}
}
