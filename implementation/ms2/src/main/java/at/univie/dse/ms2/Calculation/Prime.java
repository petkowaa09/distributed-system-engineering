package at.univie.dse.ms2.Calculation;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

/**
 * Class for Prime calculation
 *
 * @author Roman Szabo, 01638811
 */

class Prime {

	/**
	 * Determine if given number is prime.
	 *
	 * @param request
	 *            List<BigDecimal>
	 * @return boolean result
	 */
	boolean calculate(List<BigDecimal> request) {
		BigInteger checkPrime = request.get(0).toBigInteger();
		for (BigInteger i = new BigInteger("2"); i.compareTo(checkPrime.divide(new BigInteger("2"))) < 0; i = i
				.add(new BigInteger("1"))) {
			if (checkPrime.mod(i).compareTo(new BigInteger("0")) == 0) {
				return false;
			}
		}
		return true;
	}

}
