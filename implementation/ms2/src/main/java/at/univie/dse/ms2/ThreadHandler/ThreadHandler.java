package at.univie.dse.ms2.ThreadHandler;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import at.univie.dse.ms2.Calculation.Calculation;
import model.ServiceType;

/**
 * Class for handling threads and multithreaded calculations
 *
 * @author Roman Szabo, 01638811
 */
public class ThreadHandler {

	private static final Logger LOG = LoggerFactory.getLogger(ThreadHandler.class);

	private ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newCachedThreadPool();

	private Map<String, Exception> exceptionMap = new HashMap<>();

	private Calculation calculation = new Calculation();

	public ThreadHandler() {
	}

	/**
	 * Processing calculation - create new calculation, obtain calculation ID -
	 * submit new calculation to Thread Pool - return calculation ID
	 *
	 * @param type
	 *            ServiceType - calculation type
	 * @param request
	 *            request List<BigDecimal> from CalculationRequest
	 * @return calculation ID
	 */
	public String processRequest(ServiceType type, List<BigDecimal> request) {
		String calculationId = calculation.createCalculation(type, request);
		threadPool.submit(() -> {
			try {
				calculation.fireCalculation(calculationId);
			} catch (IOException e) {
				exceptionMap.put(calculationId, e);
				LOG.error("Calculation error for calculationId: {} is {}: ", calculationId, e.getMessage());
			}
		});
		return calculationId;
	}

	/**
	 * Return result for requested calculation ID. - Check for calculation errors
	 * returned from thread in exception map - Calls method from Calculation class
	 * and return result
	 * 
	 * @param calculationId
	 *            calculation Id
	 * @return Exception or Result
	 */
	public Object getResult(String calculationId) {
		if (exceptionMap.containsKey(calculationId)) {
			return exceptionMap.get(calculationId);
		} else {
			return calculation.getResult(calculationId);
		}
	}

	/**
	 * Get running threads in Thread Pool.
	 * 
	 * @return number of running threads
	 */
	public int getRunningThreads() {
		int loadApproximation = threadPool.getActiveCount() + threadPool.getQueue().size()
				+ calculation.numOfNotRetrievedCalculations();
		LOG.info("Approximated load is {}.", loadApproximation);
		return loadApproximation;
	}

}
