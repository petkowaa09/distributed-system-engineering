package at.univie.dse.ms2;

import at.univie.dse.ms2.ThreadHandler.ThreadHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Java-based configuration for this service.
 * 
 * @author Roman Szabo, 01638811
 *
 */
@Configuration
public class AppConfig {

	@Bean
	public ThreadHandler threadHandler() {
		return new ThreadHandler();
	}

	@Bean
	public ServiceUtils serviceUtils() {
		return new ServiceUtils();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
