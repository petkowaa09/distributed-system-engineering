package at.univie.dse.ms2.Calculation;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import model.ServiceType;

/**
 * Main calculation controller. Holds individual calculation.
 *
 * @author Roman Szabo, 01638811
 */

public class Calculation {

	/**
	 * Internal Data Model for Calculation storage in calculation map.
	 */
	class CalculationData {
		private String calculationId;
		private ServiceType type;
		private List<BigDecimal> request;
		private Object result;

		CalculationData(ServiceType type, List<BigDecimal> request) {
			calculationId = UUID.randomUUID().toString();
			this.type = type;
			this.request = request;
		}
	}

	private Map<String, CalculationData> calculations = new HashMap<>();

	public Calculation() {
	}

	/**
	 * Fire calculation method. Call calculation method based on Request
	 * 
	 * @param calculationId
	 *            calculation Id to find created calculation.
	 * @throws IOException
	 *             from calculations
	 */
	public void fireCalculation(String calculationId) throws IOException {
		CalculationData calculationData = calculations.get(calculationId);
		switch (calculationData.type) {
		case Addition:
			calculationData.result = new Addition().calculate(calculationData.request);
			break;
		case Multiplication:
			calculationData.result = new Multiplication().calculate(calculationData.request);
			break;
		case Prime:
			calculationData.result = new Prime().calculate(calculationData.request);
			break;
		case Fibonacci:
			calculationData.result = new Fibonacci().calculate(calculationData.request);
			break;
		default:
			throw new IOException("Invalid operation type");
		}
		calculations.put(calculationId, calculationData);
	}

	/**
	 * Create new calculation. Just store data in calculation map. For execute, call
	 * fireCalculation.
	 * 
	 * @param type
	 *            ServiceType
	 * @param request
	 *            List<BigDecimal> from request
	 * @return calculation Id
	 */
	public String createCalculation(ServiceType type, List<BigDecimal> request) {
		CalculationData c = new CalculationData(type, request);
		String cid = c.calculationId;
		calculations.put(cid, c);
		return cid;
	}

	/**
	 * Obtain Result of calculation.
	 * 
	 * @param calculationId
	 *            calculation Id
	 * @return result or Exception if result not present
	 */
	public Object getResult(String calculationId) {
		if (!calculations.containsKey(calculationId)) {
			return new Exception("Calculation ID NOT Found.");
		}
		Object result = calculations.get(calculationId).result;
		calculations.remove(calculationId);
		return result;
	}

	/**
	 * Return number of all calculations.
	 * 
	 * @return calculation count
	 */
	public int numOfNotRetrievedCalculations() {
		return calculations.size();
	}

}
